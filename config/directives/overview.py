
from datetime import datetime
import os

import docutils
from docutils.parsers.rst import Directive
import docutils.nodes
from docutils.parsers.rst import directives

import labnote.operator


def register():
    directives.register_directive("overview", Overview)


class Overview(Directive):

    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True
    has_content = False
    option_spec = {
        "depth": directives.nonnegative_int
    }

    def run(self):
        op = labnote.operator.Operator.ineedanexit()
        self.log = op.log

        # get current dir
        ident = op.cur.ident
        fp = op.model.tabs[ident]["file"]
        cd = os.path.dirname(fp)

        # get start dir
        d = self.arguments[0]
        dp = os.path.join(cd, d)

        dp = os.path.normpath(dp)

        # get max depth
        depth = self.options.get("depth", 3)

        # create doctree elements
        nodes = []

        op.log.debug("crawling %s", dp)
        n = self.walk(dp, depth, cd)
        if n:
            bl = docutils.nodes.bullet_list(bullet="-")
            for test in n:
                #self.log.debug(str(test))
                bl.append(test)
            nodes.append(bl)

        return nodes

    def walk(self, dp, depth, cd):
        nodes = []

        if not os.path.isdir(dp):
            return nodes

        #for de in os.scandir(dp):
        for de in sorted(os.scandir(dp), key=lambda x: x.name, reverse=True):
            if de.is_dir():
                #self.log.debug("dir %s", de.name)

                ref = docutils.nodes.reference()
                ref += docutils.nodes.Text(de.name)
                ref["refuri"] = os.path.relpath(de.path, start=cd)

                p = docutils.nodes.paragraph()
                p += ref

                li = docutils.nodes.list_item()
                li += p


                bl = docutils.nodes.bullet_list(bullet="-")
                li += bl

                nodes.append(li)

                if depth:
                    subtree = self.walk(de.path, depth-1, cd)

                    for n in subtree:
                        bl.append(n)
            else:
                #self.log.debug("file %s", de.name)

                if not de.name.endswith(".rst"):
                    continue

                ref = docutils.nodes.reference()
                ref += docutils.nodes.Text(de.name)
                ref["refuri"] = os.path.relpath(de.path, start=cd)

                p = docutils.nodes.paragraph()
                p += ref

                li = docutils.nodes.list_item()
                li += p

                nodes.append(li)

        return nodes

    def create_entry(self, link, text=""):
        #entry = docutils.nodes.section()
        p = docutils.nodes.paragraph()
        #title = docutils.nodes.title()
        ref = docutils.nodes.reference()
        ref += docutils.nodes.Text(text)
        ref["refuri"] = link
        #title += ref
        #entry += title
        p += ref
        return p



