#!/usr/bin/env python3

# This file is part of LabNote2
# Copyright 2019-2021 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import time
time_start = time.process_time_ns()

import argparse
import logging
import os
import signal
import sys
import tempfile
import threading
import traceback

import labnote.base
import labnote.operator
import labnote.events


def main(log, settings):
    settings["time_start"] = time_start

    check_optional_modules(settings)

    if not settings["webkit"]:
        settings["renderer"] = "gtk"

    global operator
    operator = labnote.operator.Operator(log, settings)

    files = settings["files"]

    if not files:
        files = [settings["path"]]

    fps = []
    for f in files:
        fp = prepare_fp(f)
        fps.append(fp)

    basepath, filepath = os.path.split(fps[0])

    log.debug("changing to %s", basepath)
    os.chdir(basepath)
    operator.model.set_base(basepath)
    operator.model.set_base_file(filepath)

    if settings["auto-git"]:
        operator.enqueue(labnote.events.GITinit())
    if settings["git-pull"]:
        operator.enqueue(labnote.events.GITpull())

    config_dps = config_paths()

    plugin_dps = [os.path.join(dp, "directives") for dp in config_dps]
    operator.enqueue(labnote.events.LoadPlugins(plugin_dps))

    template_dps = [os.path.join(dp, "templates") for dp in config_dps]
    operator.enqueue(labnote.events.LoadTemplates(template_dps))

    style_dps = [os.path.join(dp, "styles") for dp in config_dps]
    fn = settings["style-gtkview"]
    if not os.path.isabs(fn):
        for dp in style_dps:
            fp = os.path.join(dp, fn)
            if os.path.exists(fp):
                settings["style-gtkview"] = fp
                break
    fn = settings["style-webview"]
    if not os.path.isabs(fn):
        for dp in style_dps:
            fp = os.path.join(dp, fn)
            if os.path.exists(fp):
                settings["style-webview"] = fp
                break

    for fp in fps:
        operator.enqueue(labnote.events.OpenTab(fp))

    operator.run()


def stop():
    operator.queue.put(labnote.events.Quit())


def config_paths():
    name = "labnote2"
    config_dps = []

    dp_user = os.getenv("XDG_CONFIG_HOME")
    if not dp_user:
        # seems to use $HOME from env
        dp_user = os.path.expanduser("~/.config")
    dp_user = os.path.join(dp_user, name)

    if os.path.isdir(dp_user):
        config_dps.append(dp_user)

    dp_sys = os.path.join("/etc", name)

    if os.path.isdir(dp_sys):
        config_dps.append(dp_sys)

    dp_inst = os.path.dirname(os.path.realpath(__file__))
    dp_inst = os.path.join(dp_inst, "../config")
    dp_inst = os.path.normpath(dp_inst)

    if os.path.isdir(dp_inst):
        config_dps.append(dp_inst)

    return config_dps


def prepare_fp(filepath):

    # TODO use sth more reasonable? dont use special filenames
    if filepath == "/INTRO":
        tempdir = tempfile.mkdtemp(prefix="labnote2")
        filepath = os.path.join(tempdir, "index.rst")

        with open(filepath, "w") as tmp:
            tmp.write("\nWelcome to LabNote2!\n\n")

    filepath = os.path.normpath(filepath)
    filepath = os.path.expanduser(filepath)
    filepath = os.path.abspath(filepath)

    if not filepath.endswith(".rst") and not filepath.endswith(".md"):
        filepath = os.path.join(filepath, "index.rst")

    return filepath


def check_optional_modules(settings):
    import gi

    try:
        gi.require_version("Spelling", "1")
        from gi.repository import Spelling
        settings["spellcheck"] = True
    except Exception:
        settings["spellcheck"] = False

    try:
        from myst_parser.docutils_ import Parser as MyST
        settings["markdown"] = True
    except Exception:
        settings["markdown"] = False

    try:
        gi.require_version("WebKit", "6.0")
        from gi.repository import WebKit
        settings["webkit"] = True
    except Exception:
        settings["webkit"] = False


"""LabNote2
The structure of this progam follows the MOVE pattern.
"""
if __name__ == "__main__":
    scheme = {
        "path": {"type": "path", "default": "/INTRO"},
        "files": {"type": "list", "default": [], "positional": "optlist"},
        "template-default": {"type": "str", "default": ""},
        "auto-git": {"type": "bool", "default": False, "desc": "auto commit and push"},
        "git-pull": {"type": "bool", "default": False, "desc": "pull from git repo on startup"},
        "editor-right": {"type": "bool", "default": False},
        "renderer": {"type": "str", "default": "webkit", "desc": "webkit or gtk"},
        #"font-mono": {"type": "str", "default": "Liberation Mono Regular 10"},
        #"font-sans": {"type": "str", "default": "Liberation Sans 11"},
        "font-mono": {"type": "str", "default": None},
        "font-sans": {"type": "str", "default": None},
        "style-sourceview": {"type": "str", "default": "oblivion"},
        "style-webview": {"type": "str", "default": "oblivion.css"},
        "style-gtkview": {"type": "str", "default": "gtk.css"},
        "latex-bin": {"type": "str", "default": "pdflatex", "desc": "set to miktex pdflatex.exe path for windows"},
        "adaptive": {"type": "bool", "default": False, "desc": "mode intended for small screens (eg pinephone"},
        "benchmark": {"type": "bool", "default": False, "desc": "print timings for profiling"},
        "scroll": {"type": "bool", "default": False, "desc": "auto scroll to edited line"}
    }

    app = labnote.base.Application("labnote2", scheme, main, stop)
    app.run()


