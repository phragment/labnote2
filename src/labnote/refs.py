# This file is part of LabNote2
# Copyright 2019-2024 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import mimetypes
import os
import urllib
import sys

# labnote link handling
# - references
# - uris
# - filepaths

def is_image(uri):
    # guess filetype by suffix
    typ, enc = mimetypes.guess_type(uri, strict=False)
    #print("type {} encode {}".format(typ, enc))
    if not typ:
        # missing or unknown suffix
        # probably text file?
        typ = "text/plain"
    if typ.startswith("image/"):
        return True
    return False


def is_video(uri):
    typ, enc = mimetypes.guess_type(uri, strict=False)
    if not typ:
        typ = "text/plain"
    if typ.startswith("video/"):
        return True
    return False


def shorten_user(fp):
    # inverse os.path.expanduser
    # used for displaying the start dir
    if os.path.isabs(fp):
        home = os.path.expanduser("~")
        drive, home = os.path.splitdrive(home)  # windows
        home = os.path.normpath(home)  # windows

        if fp.startswith(home):
            fp = fp.replace(home, "~")
    return fp


def minify_fp2ref(fp, basedir, curdir):
    """
    used for inserting of uris into text
    generate relative reference if possible
    """

    # fp
    # basedir, absolute (starts with /)
    # curdir, relative

    if fp.startswith(basedir):
        fp = os.path.relpath(fp, start=basedir)

        if fp.startswith(curdir):
            fp = os.path.relpath(fp, start=curdir)
        else:
            fp = "/" + fp

    else:
        fp = shorten_user(fp)
        return "file://" + fp

    return fp


def make_reference(uri, target=""):
    # TODO move to own file
    # TODO escape whitespace?
    uri = uri.replace(" ", "\ ")

    if is_image(uri):
        if not target:
            target = uri
        ref = ".. figure:: {}\n  :target: {}\n\n  description\n\n".format(uri, target)
    else:
        ref = "`<{}>`__\n\n".format(uri)

    return ref


def make_literal_block(text):
    # TODO move to own file
    # decision: dont insert mark, to allow fast append to blocks
    #lines = ["::\n\n"]
    lines = []
    for line in text.splitlines():
        if line.strip():
            line = "  " + line
        lines.append(line)
    text = "\n".join(lines)
    return text


def shorten_ref(ref, len_max=15):
    """
    try to shorten by
    1. discarding index.rst as filename
    2. discarding leading directories one by one
    3. truncating filename
    """

    if len(ref) <= len_max:
        return ref

    parts = ref.split("/")

    if parts[-1] == "index.rst":
        parts.pop(-1)

        if len("/".join(parts)) <= len_max:
            return "/".join(parts) + "/"

    n = len(parts)
    for i in range(n-1):
        parts.pop(0)

        if len("/".join(parts)) <= len_max:
            return "…/" + "/".join(parts)

    return parts[-1][0:len_max] + "…"


def convert_ref2laburi(ref, cd, scheme="labnote"):
    # TODO let it fail and handle further up, while parsing/converting?
    if not ref:
        return ""

    if "://" in ref:
        if not ref.startswith("file"):
            return ref

    fp = convert_ref2fp(ref, cd)
    fp = urllib.parse.quote(fp)
    uri = "{}://{}".format(scheme, fp)

    return uri


def convert_laburi2fp(uri, scheme="labnote"):
    scheme += "://"
    if uri.startswith(scheme):
        fp = uri[len(scheme):]
        fp = urllib.parse.unquote(fp)
        return fp

    return uri


def convert_ref2fp(ref, cd):
    """
    foo.rst          relative to current directory
    /foo.rst         absolute to start directory
    file://foo.rst   absolute to start directory
    file:///foo.rst  absolute to root directory

    resolve ~ to home directory
    normalize .. (allow traversal out of start directory?)

    passthrough for uris (http, ftp, etc)

    return file path as uri relative to start directory (cwd) or absolute
    """

    if ref.startswith("file://"):
        fp = ref[7:]
    elif ref.startswith("/"):
        fp = ref
    else:
        fp = os.path.join(cd, ref)

    fp = os.path.expanduser(fp)
    fp = os.path.normpath(fp)

    return fp



