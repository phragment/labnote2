# This file is part of LabNote2
# Copyright 2019-2024 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import datetime
import enum
import os
import random
import string
import sys
import tempfile
import threading
import time
import urllib

import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, Gdk, GLib, Gio, Pango, GdkPixbuf  # noqa

gi.require_version("GtkSource", "5")
from gi.repository import GtkSource  # noqa

try:
    gi.require_version("Spelling", "1")
    from gi.repository import Spelling  # noqa
except Exception:
    pass

import labnote.events  # noqa
import labnote.transform
import labnote.utils
import labnote.refs

import labnote.gtkview
import labnote.webview

# move to ext file?
# https://docutils.sourceforge.io/docs/ref/rst/directives.html
HELP = {
    "Journal Link": "`<{today}/index.rst>`_\n\n",
    "Reference": "`<.rst>`__\n\n",
    "Header": ":title: Title\n:subtitle: Subtitle\n:author: Authors\n:date: 1900-01-01\n:topic: Topic\n\n",
    "Title": "=====\nTitle\n=====\n",
    "Subtitle": "--------\nSubtitle\n--------\n",
    "Section": "Section\n=======\n",
    "Subsection": "Subsection\n----------\n",
    "Subsubsection": "Subsubsection\n~~~~~~~~~~~~~\n",
    "ToC": ".. contents:: Table of Contents\n\n",
    "Equation": ".. math::\n\n  a \cdot b = c\n\n",
    "Figure": ".. figure:: <img>\n  :target: <img>\n\n  caption\n",
    "CSV Table": ".. csv-table:: <title>\n  :header: \"Foo\", \"Bar\"\n\n  Foo, Bar\n\n",
    "CSV Input": ".. csv-table:: <title>\n  :header-rows: 1\n  :file: <input.csv>\n\n"
}


class ViewMode(enum.Enum):
    BOTH = 1
    EDIT = 2
    VIEW = 4


# decorator for enqueuing functions into gtk main thread
def gtk(func):
    def wrapper(*args, **kwargs):
        #print("view wrapper, called by", threading.current_thread().name)
        GLib.idle_add(func, *args, **kwargs)
    return wrapper


class View:

    def __init__(self, log, config, operator):
        self.log = log.getChild(type(self).__name__)
        self.config = config
        self.operator = operator

        self.schemes = {}

        self.update_started = time.time()

        self.window = None  # set in MainWindow.__init__
        self.app = Application(self)

    def run(self):
        self.log.debug("starting in " + threading.current_thread().name)
        self.app.run()

    @gtk
    def stop(self):
        for tab in self.window.tabs:
            self.window.tabs[tab].viewer.stop()

        # keep clipboard data, not needed?
        #clipboard.store()

        self.app.win.window.destroy()

    @gtk
    def set_win_title(self, title):
        self.window.set_title(title)

    @gtk
    def create_tab(self, ident, name):
        self.window.create_tab(ident, name)

    @gtk
    def close_tab(self, ident):
        self.window.close_tab(ident)

    @gtk
    def update_tab(self, ident, name, text, pos):
        """
        used to change current file of tab

        pos = -1  keep current scroll pos
               0  scroll to start of file
               x  scroll to lineno x
        """

        tab = self.window.tabs[ident]
        tab.set_name(name)
        tab.viewer.clear()
        tab.source.set_text(text, pos)

    #@gtk
    def get_text(self, ident):
        return self.window.tabs[ident].source.get_text()

    def buffer_is_modified(self, ident):
        return self.window.tabs[ident].source.buffer.get_modified()

    def buffer_unmodify(self, ident):
        tab = self.window.tabs[ident]
        tab.source.buffer.set_modified(False)

    @gtk
    def load_html(self, ident, html):
        self.window.tabs[ident].viewer.load_html(html)

    @gtk
    def update_gtkview(self, ident, entries, line):
        self.window.tabs[ident].viewer.update(entries, line)

    @gtk
    def next_tab(self, ident):
        self.window.next_tab()

    @gtk
    def prev_tab(self, ident):
        self.window.prev_tab()

    @gtk
    def create_export_menu(self, options, selection):
        self.window.toolbar.create_export_menu(options, selection)

    @gtk
    def create_templates_menu(self, options):
        self.window.toolbar.create_templates_menu(options)

    @gtk
    def status_set(self, status):
        tab = self.window.get_active_tab()
        if tab:
            tab.statusbar.set(status)

    @gtk
    def status_set_timeout(self, status, timeout=5):
        tab = self.window.get_active_tab()
        if tab:
            tab.statusbar.set_timeout(status, timeout)

    @gtk
    def status_clear(self):
        tab = self.window.get_active_tab()
        if tab:
            tab.statusbar.clear()

    @gtk
    def info_ask(self, text, events):
        self.window.infobar.ask(text, events)

    @gtk
    def focus_editor(self):
        tab = self.window.get_active_tab()
        if tab:
            tab.source.sourceview.grab_focus()

    @gtk
    def focus_viewer(self):
        tab = self.window.get_active_tab()
        if tab:
            tab.viewer.list_view.grab_focus()

    @gtk
    def scroll_editor(self, ident, line):
        pass

    @gtk
    def scroll_viewer(self, ident, line):
        #tab = self.window.get_active_tab()
        tab = self.window.tabs[ident]
        if tab:
            line = tab.source.get_pos()
            self.log.debug("scrolling viewer to line %s", line)
            tab.viewer.scroll_to_line(line)

    @gtk
    def search_add(self, ident, result):
        tab = self.window.tabs[ident]
        tab.searchsidebar.add(result)

    @gtk
    def search_clear(self, ident):
        tab = self.window.tabs[ident]
        tab.searchsidebar.clear()

    def register_scheme(self, scheme, cb):
        self.schemes[scheme] = cb

    @gtk
    def dialog_save_as(self, ident):
        self.window.toolbar.on_save_as(None, ident)


class Application:

    def __init__(self, view):
        self.view = view
        self.app = Gtk.Application.new(None, 0)

        # https://developer.gnome.org/documentation/tutorials/application-id.html
        # this seems to enforce a single instance... what the fudge?
        appid = "de.elektronenpumpe.labnote2.pid{}".format(os.getpid())
        self.app.set_property("application-id", appid)

        self.app.connect("activate", self.activate)

    def run(self):
        self.app.run()

    def activate(self, app):
        self.view.log.debug("app activate")
        self.win = MainWindow(app, self.view)


class MainWindow:

    def __init__(self, app, view):

        view.window = self

        self.view = view
        self.log = self.view.log.getChild(type(self).__name__)
        self.config = self.view.config
        self.operator = self.view.operator

        self.tabs = {}

        self.window = Gtk.ApplicationWindow.new(app)

        if self.config["adaptive"]:
            self.setup_headerbar()
            self.mode = ViewMode.VIEW
        else:
            self.mode = ViewMode.BOTH

        self.window.set_icon_name("text-x-generic-symbolic")
        self.set_window_size()
        self.set_title(None)

        self.window.connect("close-request", self.on_window_close_request)

        ec_key = Gtk.EventControllerKey()
        ec_key.set_propagation_phase(Gtk.PropagationPhase.CAPTURE)
        ec_key.connect("key-pressed", self.on_key_pressed)
        self.window.add_controller(ec_key)

        ec_gc = Gtk.GestureClick()
        ec_gc.props.button = 8
        ec_gc.connect("pressed", self.on_button_back)
        self.window.add_controller(ec_gc)

        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.window.set_child(self.vbox)

        self.toolbar = Toolbar(self.view)
        if not self.config["adaptive"]:
            self.vbox.append(self.toolbar.get_wdgt())

        self.hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.hbox.props.vexpand = True

        self.notebook = ModdedNotebook()

        self.hbox.append(self.notebook.get_widget())

        self.vbox.append(self.hbox)

        self.infobar = Infobar(self.view)
        self.vbox.append(self.infobar.get_wdgt())

        self.check_fonts()

        self.window.set_visible(True)

        if self.config["adaptive"]:
            self.notebook.set_show_tabs(False)

        if self.config["benchmark"]:
            time_stop = time.process_time_ns()
            time_start = self.config["time_start"]
            print("startup done", (time_stop - time_start)/1000000, "ms")

    def get_wdgt(self):
        return self.window

    def set_mode(self, mode):
        if self.mode == mode:
            return

        self.mode = mode

        for ident, tab in self.tabs.items():
            tab.show(self.mode)

        # TODO change size request
        self.set_window_size()

        if mode == ViewMode.VIEW:
            tab = self.get_active_tab()
            if tab:
                tab.source.update()

    def get_mode(self):
        return self.mode

    def create_tab(self, ident, name):
        tab = Tab(self.view, ident, name)
        self.tabs[ident] = tab

        self.notebook.add_tab(tab)

        # TODO also set cursor (to line, including scroll to line)?
        self.operator.enqueue(labnote.events.FocusEditor())

    def close_tab(self, ident):
        self.log.debug("closing tab %s", ident)
        tab = self.tabs[ident]
        tab.viewer.stop()
        self.notebook.remove_tab(tab)
        del tab.hbox
        del self.tabs[ident]

    def next_tab(self):
        self.notebook.next_tab()

    def prev_tab(self):
        self.notebook.prev_tab()

    def get_active_tab_id(self):
        return self.notebook.get_active_tab_id()

    def get_active_tab(self):
        ident = self.get_active_tab_id()
        if ident:
            return self.tabs[ident]
        else:
            return None

    def set_window_size(self):
        display = self.window.get_display()

        # 'GdkWaylandDisplay' object has no attribute 'get_primary_monitor'
        #monitor = display.get_primary_monitor()  # none if not user configured!
        #if not monitor:
        #    monitor = display.get_monitor(0)

        monitors = display.get_monitors()
        monitor = monitors[0]

        geo = monitor.get_geometry()
        #print(geo.width, geo.height)

        if geo.height > geo.width:
            self.log.info("suggestion: use convergence mode")
            return

        # GDK_SCALE
        sf = self.window.get_scale_factor()

        width = int(geo.width * sf * 0.8)
        height = int(geo.height * sf * 0.8)

        if self.mode != ViewMode.BOTH:
            width /= 2

        if self.config["adaptive"]:
            if geo.width < width:
                self.window.maximize()
                return

        self.window.set_default_size(width, height)

    def setup_headerbar(self):
        # TODO move headerbar to own class
        # unify with toolbar?

        self.headerbar = Gtk.HeaderBar()
        self.window.set_titlebar(self.headerbar)

        self.box_title = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.label_title = Gtk.Label.new("LabNote2")
        self.box_title.append(self.label_title)

        self.label_subtitle = Gtk.Label()
        self.box_title.append(self.label_subtitle)

        self.headerbar.set_title_widget(self.box_title)

        self.headerbar.set_show_title_buttons(False)

        #icon = Gtk.Image.new_from_icon_name("window-close", Gtk.IconSize.MENU)
        #button_close = Gtk.ToolButton.new(icon, "Close")
        #button_close.connect("clicked", self.on_button_close_clicked)
        #self.headerbar.pack_end(button_close)

        button_back = Gtk.Button.new_from_icon_name("go-previous")
        button_back.set_tooltip_text("Go Back")
        button_back.connect("clicked", self.on_hbb_back_clicked)
        self.headerbar.pack_start(button_back)

        button_home = Gtk.Button.new_from_icon_name("go-home")
        button_back.set_tooltip_text("Go Home")
        button_home.connect("clicked", self.on_hbb_home_clicked)
        self.headerbar.pack_start(button_home)

        # document-edit <> text-x-generic
        # OR
        # view-dual
        #
        # view-more (overflow, burger menu)
        # open-menu

        self.button_overflow = Gtk.Button.new_from_icon_name("view-more")
        self.button_overflow.set_tooltip_text("Overflow")
        self.button_overflow.connect("clicked", self.on_hbb_overflow_clicked)
        self.headerbar.pack_end(self.button_overflow)

        self.button_switch = Gtk.Button.new_from_icon_name("view-dual")
        self.button_switch.set_tooltip_text("Switch")
        self.button_switch.connect("clicked", self.on_hbb_switch_clicked)
        self.headerbar.pack_end(self.button_switch)

        self.button_tabs = Gtk.Button.new_from_icon_name("view-list-bullet")
        self.button_tabs.set_tooltip_text("Show Tabs")
        self.button_tabs.connect("clicked", self.on_hbb_tabs_clicked)
        self.headerbar.pack_end(self.button_tabs)

        self.setup_overflow_menu()

    def on_hbb_back_clicked(self, button):
        ident = self.view.window.get_active_tab_id()
        self.operator.enqueue(labnote.events.GoBack(ident))

    def on_hbb_home_clicked(self, button):
        ident = self.view.window.get_active_tab_id()
        self.operator.enqueue(labnote.events.GoHome(ident))

    def on_hbb_tabs_clicked(self, button):
        if self.notebook.get_show_tabs():
            self.notebook.set_show_tabs(False)
        else:
            self.notebook.set_show_tabs(True)

    def on_hbb_switch_clicked(self, button):
        if self.mode == ViewMode.VIEW:
            self.set_mode(ViewMode.EDIT)
        else:
            self.set_mode(ViewMode.VIEW)

    def on_hbb_overflow_clicked(self, button):
        self.popover.popup()

    def setup_overflow_menu(self):
        # TODO unify with toolbar?

        self.popover = Gtk.PopoverMenu()
        self.popover.set_parent(self.button_overflow)
        self.popover.set_position(Gtk.PositionType.BOTTOM)
        self.popover.props.autohide = True

        self.pobox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.popover.set_child(self.pobox)

        #button_save = self.create_button("Save", "document-save", self.on_po_search_clicked)
        button_save = self.create_button("Save", "media-floppy", self.on_po_save_clicked)
        self.pobox.append(button_save)

        button_paste = self.create_button("Paste", "edit-paste", self.on_po_paste_clicked)
        self.pobox.append(button_paste)

        button_search = self.create_button("Search", "edit-find", self.on_po_search_clicked)
        self.pobox.append(button_search)

        button_search_global = self.create_button("Search File", "system-search", self.on_po_search_global_clicked)
        self.pobox.append(button_search_global)

        button_exit = self.create_button("Exit", "window-close", self.on_po_exit_clicked)
        self.pobox.append(button_exit)

    def create_button(self, label_text, icon_name, cb):
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        icon = Gtk.Image.new_from_icon_name(icon_name)
        box.append(icon)

        label = Gtk.Label.new(label_text)
        label.set_xalign(0)
        label.props.hexpand = True
        label.props.margin_start = 5
        box.append(label)

        button = Gtk.Button.new()
        button.add_css_class("flat")
        button.set_child(box)

        button.connect("clicked", cb)

        return button

    def on_po_exit_clicked(self, button):
        self.window.close()

    def on_po_save_clicked(self, button):
        ident = self.get_active_tab_id()
        self.operator.enqueue(labnote.events.Save(ident))

    def on_po_paste_clicked(self, button):
        # FIXME this should not be necessary
        tab = self.get_active_tab()
        clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        tab.source.buffer.paste_clipboard(clipboard, None, True)

    def on_po_search_clicked(self, button):
        tab = self.get_active_tab()
        tab.searchbar_local.show()

    def on_po_search_global_clicked(self, button):
        tab = self.get_active_tab()
        tab.searchbar_global.show()

    def set_title(self, title):
        if self.config["adaptive"]:
            if title:
                self.label_subtitle.set_text(title)
        else:
            if title:
                title = labnote.refs.shorten_ref(title)
                self.window.set_title("{} - LabNote2".format(title))
            else:
                self.window.set_title("LabNote2")

    def check_fonts(self):
        defaults = self.get_default_fonts()

        for type_, desc in defaults.items():
            if not self.view.config.get(type_, None):
                self.view.config[type_] = desc

        # orig
        #for font in self.view.config["fonts"]:
        #    if not self.view.config["fonts"][font]:
        #        self.view.config["fonts"][font] = defaults[font]

        #self.log.debug("fonts: {}".format(self.view.config["fonts"]))

    def get_default_fonts(self):
        # TODO get font scale?
        mono = ""
        sans = ""

        # get from gtk (xfconf?)
        pango_context = self.window.get_pango_context()
        font = pango_context.get_font_description()
        family = font.get_family()
        size = int(font.get_size() / Pango.SCALE)
        self.log.debug("Font GTK: {} {}".format(family, size))
        sans = "{} {}".format(family, size)

        # get from gconf/dconf
        sss = Gio.SettingsSchemaSource.get_default()
        schema = sss.lookup("org.gnome.desktop.interface", True)
        if schema:
            settings = Gio.Settings(settings_schema=schema)
            mono = settings.get_string("monospace-font-name")
            self.log.debug("Font GNOME monospace: {}".format(mono))
            sans = settings.get_string("font-name")
            self.log.debug("Font GNOME: {}".format(sans))

        return {"font-mono": mono, "font-sans": sans}

    def on_window_close_request(self, window):
        self.operator.enqueue(labnote.events.Quit())
        # return event handled, otherwise window shuts down immediately
        return True

    def on_button_back(self, ec, press, x, y):
        # go back
        ident = self.get_active_tab_id()
        self.operator.enqueue(labnote.events.GoBack(ident))
        return True

    def on_key_pressed(self, ec, keyval, keycode, state):

        if state & Gdk.ModifierType.CONTROL_MASK:

            # Ctrl Tab
            if keyval == Gdk.KEY_Tab:
                # switch to next tab
                ident = self.get_active_tab_id()
                self.operator.enqueue(labnote.events.NextTab(ident))
                return True

            # Ctrl Shift Tab
            if keyval == Gdk.KEY_ISO_Left_Tab:
                # switch to previous tab
                ident = self.get_active_tab_id()
                self.operator.enqueue(labnote.events.PrevTab(ident))
                return True

            # Ctrl s
            if keyval == Gdk.KEY_s:
                # save
                ident = self.get_active_tab_id()
                self.operator.enqueue(labnote.events.Save(ident))
                return True

            # Ctrl Shift e
            if keyval == Gdk.KEY_E:
                # export to PDF
                ident = self.get_active_tab_id()
                template = self.toolbar.get_selected_template()
                self.operator.enqueue(labnote.events.Export(ident, template))
                return True

            # Ctrl f
            if keyval == Gdk.KEY_f:
                # search in file
                tab = self.get_active_tab()
                #tab.searchbar.show()
                tab.searchbar_local.show()
                return True

            # Ctrl Shift f
            if keyval == Gdk.KEY_F:
                # search over files
                #self.searchbar.show()
                tab = self.get_active_tab()
                if tab:
                    tab.searchbar_global.show()
                return True

            # Ctrl w
            if keyval == Gdk.KEY_w:
                # close current tab
                ident = self.get_active_tab_id()
                if ident:
                    self.operator.enqueue(labnote.events.CloseTab(ident))
                else:
                    # no tab open
                    self.operator.enqueue(labnote.events.Quit())
                return True

            # Ctrl r
            if keyval == Gdk.KEY_r:
                self.log.debug("reloading")
                # reload cached
                tab = self.get_active_tab()
                if tab:
                    tab.viewer.clear_cache()
                    tab.source.update()
                return True

            # Ctrl Shift r
            if keyval == Gdk.KEY_R:
                self.log.debug("reread file")
                # load self
                tab = self.get_active_tab()
                if tab:
                    fp = self.operator.model.get_tab(tab.ident)["file"]
                    self.operator.enqueue(labnote.events.Open(tab.ident, fp))
                return True

            # Ctrl space
            if keyval == ord(" "):
                self.log.debug("scroll to current line")
                ident = self.get_active_tab_id()
                self.operator.enqueue(labnote.events.ScrollViewer(ident))
                return True

        if state & Gdk.ModifierType.ALT_MASK:

            # Alt Left/Right
            if keyval == Gdk.KEY_Left or keyval == Gdk.KEY_Right:
                # disable move word
                return True

            # Alt e
            if keyval == Gdk.KEY_e:
                self.operator.enqueue(labnote.events.FocusEditor())
                return True

            # Alt v
            if keyval == Gdk.KEY_v:
                self.operator.enqueue(labnote.events.FocusViewer())
                return True

        # F7
        if keyval == Gdk.KEY_F7:
            # start spellcheck
            tab = self.get_active_tab()
            tab.source.check_spelling()
            return True

        return False


class Statusbar:

    def __init__(self, view):

        self.view = view
        self.log = view.log

        self.box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.box.set_name("statusbar")

        self.filler = Gtk.Label()
        self.filler.props.hexpand = True
        self.box.append(self.filler)

        self.label_line_count = Gtk.Label()
        self.box.append(self.label_line_count)

        self.box.append(Gtk.Separator())

        self.label = Gtk.Label()
        self.box.append(self.label)

        self.box.append(Gtk.Separator())

        self.label_mod = Gtk.Label()
        self.box.append(self.label_mod)

    def get_wdgt(self):
        return self.box

    def clear(self):
        self.label.set_text("")

    def set(self, text):
        self.label.set_text(" {} ".format(text))

    def set_timeout(self, text, timeout):
        self.set(text)
        # low res timer
        GLib.timeout_add_seconds(timeout, self.on_timeout)

    def on_timeout(self):
        self.clear()

    def set_modified(self, mod):
        if mod:
            #self.label_mod.set_text("modified")
            self.label_mod.set_text(" \u2717 ")  # ballot x
        else:
            #self.label_mod.set_text("unmodified")
            self.label_mod.set_text(" \u2713 ")  # check mark

    def set_line_count(self, n):
        self.label_line_count.set_text(" {} ".format(n))


class Infobar:

    def __init__(self, view):
        # TODO save changes? cancel discard save

        self.view = view
        self.log = view.log

        self.rev = Gtk.Revealer()
        self.rev.set_name("infobar")

        self.box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.rev.set_child(self.box)

        self.label = Gtk.Label()
        self.box.append(self.label)

        self.spacer = Gtk.Label()
        self.spacer.props.hexpand = True
        self.box.append(self.spacer)

        self.button_yes = Gtk.Button.new_with_label("  OK  ")
        self.button_no = Gtk.Button.new_with_label("Cancel")

        self.button_yes.connect("clicked", self.on_button_yes)
        self.button_no.connect("clicked", self.on_button_no)

        self.box.append(self.button_no)
        self.box.append(self.button_yes)

        ec_key = Gtk.EventControllerKey()
        ec_key.connect("key-pressed", self.on_key_pressed)
        self.box.add_controller(ec_key)

        self.done()

    def get_wdgt(self):
        return self.rev

    def ask(self, text, events):
        self.events = events
        self.label.set_text(text)
        self.rev.set_reveal_child(True)
        self.button_no.grab_focus()

    def done(self):
        self.rev.set_reveal_child(False)
        self.event = None

    def on_button_yes(self, button):
        for event in self.events:
            self.view.operator.enqueue(event)
        self.done()

    def on_button_no(self, button):
        self.done()

    def on_key_pressed(self, ec, keyval, keycode, state):
        if keyval == Gdk.KEY_Escape:
            self.done()


class Toolbar:

    def __init__(self, view):
        self.view = view

        self.log = self.view.log.getChild(type(self).__name__)
        self.config = self.view.config
        self.operator = self.view.operator

        self.toolbar = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        back = self._create_button("go-previous", "Go Back")
        back.connect("clicked", self.on_back)
        self.toolbar.append(back)

        home = self._create_button("go-home", "Go Home")
        home.connect("clicked", self.on_home)
        self.toolbar.append(home)

        self.toolbar.append(Gtk.Separator())

        newf = self._create_button("document-new", "New File")
        newf.connect("clicked", self.on_new)
        self.toolbar.append(newf)

        # open in new tab
        openf = self._create_button("document-open", "Open File")
        openf.connect("clicked", self.on_open)
        self.toolbar.append(openf)

        #save = self._create_button("document-save", "Save File")
        save = self._create_button("media-floppy", "Save File")
        save.connect("clicked", self.on_save)
        self.toolbar.append(save)

        self.toolbar.append(Gtk.Separator())

        opencurdir = self._create_button("drive-harddisk", "Open Current Folder")
        opencurdir.connect("clicked", self.on_opencurdir)
        self.toolbar.append(opencurdir)

        spellcheck = self._create_button("accessories-dictionary", "Spellcheck")
        spellcheck.connect("clicked", self.on_spellcheck)
        self.toolbar.append(spellcheck)

        self.toolbar.append(Gtk.Separator())

        self.export = self._create_button("x-office-document", "Export as PDF")
        self.export.connect("clicked", self.on_export)
        self.toolbar.append(self.export)

        self.export_menu = Gtk.MenuButton.new()

        self.export_popover = Gtk.Popover()
        self.export_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.export_popover.set_child(self.export_box)
        self.export_menu.set_popover(self.export_popover)

        self.toolbar.append(self.export_menu)

        self.toolbar.append(Gtk.Separator())

        self.help = Gtk.MenuButton()
        self.help.set_icon_name("help-contents")
        self.help.set_tooltip_text("Help")
        self.toolbar.append(self.help)

        self.help_popover = Gtk.Popover()
        self.help_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.help_popover.set_child(self.help_box)
        self.help.set_popover(self.help_popover)

        self.create_help_menu()

        self.templates = Gtk.MenuButton()
        self.templates.set_icon_name("insert-object")
        self.templates.set_tooltip_text(".rst Templates")
        self.toolbar.append(self.templates)

        self.templates_popover = Gtk.Popover()
        self.templates_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.templates_popover.set_child(self.templates_box)
        self.templates.set_popover(self.templates_popover)

        self.toolbar.append(Gtk.Separator())

        self.view_mode = Gtk.MenuButton()
        self.view_mode.set_icon_name("view-dual")
        self.view_mode.set_tooltip_text("View Mode")
        self.toolbar.append(self.view_mode)

        self.view_mode_popover = Gtk.Popover()
        self.view_mode_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.view_mode_popover.set_child(self.view_mode_box)
        self.view_mode.set_popover(self.view_mode_popover)

        self.create_view_mode_menu()

        self.autoscroll = Gtk.ToggleButton()
        img = Gtk.Image.new_from_icon_name("go-jump")
        self.autoscroll.set_child(img)
        self.autoscroll.set_tooltip_text("Auto Scroll")
        self.autoscroll.connect("toggled", self.on_autoscroll)
        self.toolbar.append(self.autoscroll)

        self.setup_style()

    def create_export_menu(self, options, sel):
        # called by model
        #print(options, sel)
        self.log.debug("populating export menu")

        tb_def = Gtk.ToggleButton.new_with_label("default")
        tb_def.props.child.props.xalign = 0  # left align
        self.export_box.append(tb_def)
        tb_def.connect("toggled", self.on_export_menu_item_toggled)
        tb_def.set_active(True)
        self.export_tbs = [tb_def]

        self.templates_tex = {}
        for fp in options:
            fn = os.path.basename(fp)
            self.templates_tex[fn] = fp

            tb = Gtk.ToggleButton.new_with_label(fn)
            tb.props.child.props.xalign = 0  # left align
            tb.set_group(tb_def)
            tb.connect("toggled", self.on_export_menu_item_toggled)
            self.export_box.append(tb)
            self.export_tbs.append(tb)

            if fn == sel:
                tb.set_active(True)

    def on_export_menu_item_toggled(self, tb):
        if tb.get_active():
            self.export_selected = tb.get_label()
            self.export_popover.hide()

    def get_selected_template(self):
        return self.export_selected

    def create_view_mode_menu(self):
        tb_both = Gtk.ToggleButton.new_with_label("Live Preview")
        tb_both.connect("toggled", self.on_activate_both)
        tb_both.set_active(True)
        self.view_mode_box.append(tb_both)

        tb_edit = Gtk.ToggleButton.new_with_label("Show Editor")
        tb_edit.connect("toggled", self.on_activate_edit)
        tb_edit.set_group(tb_both)
        self.view_mode_box.append(tb_edit)

        tb_view = Gtk.ToggleButton.new_with_label("Show Viewer")
        tb_view.connect("toggled", self.on_activate_view)
        tb_view.set_group(tb_both)
        self.view_mode_box.append(tb_view)

    def create_help_menu(self):
        for name, example in HELP.items():
            button = Gtk.Button()
            button.add_css_class("flat")
            button.set_label(name)
            button.props.child.props.xalign = 0  # left align
            button.connect("clicked", self.on_help, example)
            self.help_box.append(button)

    def create_templates_menu(self, options):
        self.templates_rst = {}
        for fp in options:
            fn = os.path.basename(fp)
            self.templates_rst[fn] = fp

            button = Gtk.Button()
            button.add_css_class("flat")
            button.set_label(fn)
            button.props.child.props.xalign = 0  # left align
            button.connect("clicked", self.on_rst_template, fn)
            self.templates_box.append(button)

    def _create_button(self, icon_name, func_name):
        button = Gtk.Button.new_from_icon_name(icon_name)
        button.set_tooltip_text(func_name)
        return button

    def setup_style(self):
        #self.toolbar.set_css_name("box")  # set name, defaults to parent
        self.toolbar.set_name("mytb")  # set id
        self.toolbar.add_css_class("toolbar")  # set class

        style = """
            toolbar#mytb * {
              -gtk-icon-style: symbolic;
            }
            headerbar {
              -gtk-icon-style: symbolic;
            }
        """
        provider = Gtk.CssProvider()
        provider.load_from_data(style, -1)

        context = self.toolbar.get_style_context()
        priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        Gtk.StyleContext.add_provider(context, provider, priority)

    def get_wdgt(self):
        return self.toolbar

    def get_selected_template(self):
        for tb in self.export_tbs:
            if tb.get_active():
                fn = tb.get_label()
                return self.templates_tex[fn]
        return None

    def on_back(self, button):
        ident = self.view.window.get_active_tab_id()
        self.operator.enqueue(labnote.events.GoBack(ident))

    def on_home(self, button):
        ident = self.view.window.get_active_tab_id()
        self.operator.enqueue(labnote.events.GoHome(ident))

    def on_new(self, button):
        dialog = Gtk.FileDialog()
        #dialog.set_initial_folder()  # seems to be cwd?

        dialog.set_initial_name("index.rst")

        dialog.set_title("Create new File")

        dialog.save(self.view.window.window, None, self.on_new_response)

        # maximize on small screens
        if self.config["adaptive"]:
            dialog.maximize()

    def on_new_response(self, dialog, response):
        try:
            gf = dialog.save_finish(response)
        except GLib.GError:
            return

        fp = gf.get_path()
        if fp.endswith(".rst") or fp.endswith(".md"):
            self.operator.enqueue(labnote.events.OpenTab(fp))
        else:
            self.view.status_set_timeout("unknown file extension")

    def on_save_as(self, button, ident):
        dialog = Gtk.FileDialog()
        #dialog.set_initial_folder()  # seems to be cwd?

        dialog.set_initial_name("index.rst")

        dialog.set_title("Save as")

        dialog.save(self.view.window.window, None, self.on_save_as_response, ident)

        # maximize on small screens
        if self.config["adaptive"]:
            dialog.maximize()

    def on_save_as_response(self, dialog, response, ident):
        try:
            gf = dialog.save_finish(response)
        except GLib.GError:
            return

        fp = gf.get_path()
        if fp.endswith(".rst") or fp.endswith(".md"):
            self.operator.enqueue(labnote.events.Save(ident, fp))
        else:
            self.view.status_set_timeout("unknown file extension")

    def on_open(self, button):
        dialog = Gtk.FileDialog()

        dialog.set_title("Open File")

        dialog.open_multiple(self.view.window.window, None, self.on_open_response)

        # maximize on small screens
        if self.config["adaptive"]:
            dialog.maximize()

    def on_open_response(self, dialog, response):
        try:
            listmodel = dialog.open_multiple_finish(response)
        except GLib.GError:
            return

        num = listmodel.get_n_items()
        for i in range(num):
            fp = listmodel.get_item(i).get_path()
            if fp.endswith(".rst") or fp.endswith(".md"):  # TODO use filter?
                self.log.debug("opening %s", fp)
                self.operator.enqueue(labnote.events.OpenTab(fp))
            else:
                self.operator.enqueue(labnote.events.OpenExternal(fp))

    def on_save(self, button):
        ident = self.view.window.get_active_tab_id()
        self.operator.enqueue(labnote.events.Save(ident))

    def on_export(self, button):
        ident = self.view.window.get_active_tab_id()
        template = self.get_selected_template()
        self.operator.enqueue(labnote.events.Export(ident, template))

    def on_spellcheck(self, button):
        ident = self.view.window.get_active_tab_id()
        if not ident:
            return
        tab = self.view.window.tabs[ident]
        tab.source.check_spelling()

    def on_opencurdir(self, button):
        ident = self.view.window.get_active_tab_id()
        if not ident:
            return
        self.operator.enqueue(labnote.events.OpenCurDir(ident))

    def on_help(self, help, example):
        tab = self.view.window.get_active_tab()
        expansions = {
            "today": datetime.datetime.now().strftime("%Y-%m-%d")
        }
        tab.source.insert(example.format(**expansions))

    def on_rst_template(self, menuitem, fn):
        tf = self.templates_rst[fn]

        try:
            with open(tf, "r") as f:
                template = f.read()
        except FileNotFoundError:
            return

        tab = self.view.window.get_active_tab()
        tab.source.insert(template)

    def on_activate_both(self, menuitem):
        self.view.window.set_mode(ViewMode.BOTH)

    def on_activate_edit(self, menuitem):
        self.view.window.set_mode(ViewMode.EDIT)

    def on_activate_view(self, menuitem):
        self.view.window.set_mode(ViewMode.VIEW)

    def on_autoscroll(self, button):
        # TODO move logic to view object?
        if button.get_active():
            self.config["scroll"] = True
            ident = self.view.window.get_active_tab_id()
            self.operator.enqueue(labnote.events.ScrollViewer(ident))
        else:
            self.config["scroll"] = False


class SearchBarGlobal:

    def __init__(self, tab):
        self.tab = tab
        self.view = tab.view

        self.hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.hbox.set_name("searchbar-global")

        self.entry = Gtk.Entry()
        self.entry.props.hexpand = True
        self.hbox.append(self.entry)

        icon = self._get_icon()
        self.entry.set_icon_from_gicon(Gtk.EntryIconPosition.PRIMARY, icon)

        self.entry.set_placeholder_text("search over files")

        # TODO add buttons
        # - backward
        # - options?

        self.button_search = Gtk.Button.new_from_icon_name("system-search")
        self.button_search.connect("clicked", self.on_button_search_clicked)
        self.hbox.append(self.button_search)

        self.button_close = Gtk.Button.new_from_icon_name("window-close")
        self.button_close.connect("clicked", self.on_button_close_clicked)
        self.hbox.append(self.button_close)

        self.entry.connect("activate", self.on_activated)

        ec_key = Gtk.EventControllerKey()
        ec_key.connect("key-pressed", self.on_key_pressed)
        self.hbox.add_controller(ec_key)

    def get_wdgt(self):
        return self.hbox

    def _get_icon(self):
        gicon_emblem = Gio.ThemedIcon.new_with_default_fallbacks("emblem-system")
        emblem = Gio.Emblem.new(gicon_emblem)
        gicon = Gio.ThemedIcon.new_with_default_fallbacks("system-search")
        gicon_emblemed = Gio.EmblemedIcon.new(gicon, emblem)
        return gicon_emblemed

    def show(self):
        self.view.log.debug("search global")
        self.tab.searchbar_local.hide()
        # clear entry?
        self.hbox.show()
        self.entry.grab_focus()  # selects text by default (since 3.16?)

    def hide(self):
        self.view.operator.enqueue(labnote.events.SearchStop(self.tab.ident))

        self.hbox.hide()
        self.tab.searchsidebar.hide()

        # clear search
        #self.search("")

    def search(self, term):
        self.tab.searchsidebar.show()
        self.view.operator.enqueue(labnote.events.Search(self.tab.ident, term))

    def on_activated(self, entry):
        #self.tab.searchsidebar.show()
        term = entry.get_text()
        self.search(term)

    def on_key_pressed(self, ec, keyval, keycode, state):
        if keyval == Gdk.KEY_Escape:
            self.hide()
            self.view.operator.enqueue(labnote.events.FocusEditor())
            return True
        return False

    def on_button_search_clicked(self, button):
        term = self.entry.get_text()
        self.search(term)

    def on_button_close_clicked(self, button):
        self.hide()


class SearchSidebar:

    def __init__(self, view, tab):
        self.view = view
        self.tab = tab

        self.sw = Gtk.ScrolledWindow()
        self.sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

        self.sw.set_name("search-sidebar")

        self.liststore = Gtk.ListStore(str, int, str, str)
        self.treeview = Gtk.TreeView(model=self.liststore)
        self.treeview.set_headers_visible(False)

        self.sw.set_child(self.treeview)

        columns = ["File", "Line Number", "Line Content"]
        for (i, column) in enumerate(columns):
            cell = Gtk.CellRendererText()
            col = Gtk.TreeViewColumn(column, cell, text=i)
            col.set_sizing(Gtk.TreeViewColumnSizing.AUTOSIZE)
            self.treeview.append_column(col)

        self.treeview.connect("row-activated", self.on_row_activated)

        ec_key = Gtk.EventControllerKey()
        ec_key.connect("key-pressed", self.on_key_pressed)
        self.treeview.add_controller(ec_key)

    def get_wdgt(self):
        return self.sw

    def hide(self):
        self.sw.hide()
        # show only if shown previously
        if not self.view.window.get_mode() == ViewMode.EDIT:
            self.tab.viewer.show()

    def show(self):
        self.sw.show()
        self.tab.viewer.hide()

    def clear(self):
        self.liststore.clear()

    def add(self, result):
        self.liststore.append(result)
        # TODO grab focus only on first result
        #self.treeview.grab_focus()

    def on_row_activated(self, treeview, it, path):
        selection = treeview.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        it = model.get_iter(pathlist[0])
        res_file = model.get_value(it, 3)
        res_line = int(model.get_value(it, 1)) - 1

        #ident = self.view.window.get_active_tab_id()
        #self.view.operator.enqueue(labnote.events.Open(ident, res_file, res_line))
        self.tab.log.debug("opening tab at %s for %s", res_line, res_file)
        self.view.operator.enqueue(labnote.events.OpenTab(res_file, res_line))

    def on_key_pressed(self, ec, keyval, keycode, state):
        if keyval == Gdk.KEY_Escape:
            self.view.window.get_active_tab().searchbar_global.hide()
            self.view.operator.enqueue(labnote.events.FocusEditor())
            return True
        return False


class ModdedNotebook:

    """
    TODO
    show close button only on mouse over?

    auto select/mark tab
    auto hide/show tab bar
    """

    def __init__(self):
        self.tabs = {}
        self.visible = True
        self.autohide = True

        self.box = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)

        self.sw = Gtk.ScrolledWindow.new()
        self.sw.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        self.box.append(self.sw)

        self.bookmarks = Gtk.ListBox.new()
        self.sw.set_child(self.bookmarks)

        self.notebook = Gtk.Notebook.new()
        self.notebook.set_show_tabs(False)

        self.notebook.set_hexpand(True)
        self.box.append(self.notebook)

        self.bookmarks.connect("row-selected", self.on_listbox_row_selected)
        self.notebook.connect("switch-page", self.on_notebook_switch_page)

        self.setup_style()

        if self.autohide:
            self.bookmarks.hide()

    def get_widget(self):
        return self.box

    def add_tab(self, tab):
        self.tabs[tab.ident] = tab

        if self.autohide and len(self.tabs) > 1:
            self.set_show_tabs(True)

        label = tab.get_label()
        widget = tab.get_wdgt()

        self.notebook.append_page(widget, None)
        self.bookmarks.append(label)

        if len(self.tabs) == 1:
            row = self.bookmarks.get_row_at_index(0)
            self.bookmarks.select_row(row)

    def remove_tab(self, tab):
        #self.notebook.remove_page(page_num)
        self.notebook.detach_tab(tab.get_wdgt())
        label = tab.get_label()  # must be cached!
        listboxrow = label.get_parent()
        self.bookmarks.remove(listboxrow)
        del self.tabs[tab.ident]

        if self.autohide and len(self.tabs) < 2:
            self.set_show_tabs(False)

    def on_notebook_switch_page(self, notebook, page, n):
        # update selection
        row = self.bookmarks.get_row_at_index(n)
        self.bookmarks.select_row(row)

    def on_listbox_row_selected(self, listbox, row):
        if not row:
            return
        n = row.get_index()  # TODO
        self.notebook.set_current_page(n)

    def cur_tab(self):
        #row = self.bookmarks.get_selected_row()
        #print("FIXME", row)

        # we need to reference back to our identifier
        pos = self.notebook.get_current_page()

        ## hacky
        # get current widget
        widget = self.notebook.get_nth_page(pos)
        # iterate over self.tabs, compare tab.box pointer
        for ident, tab in self.tabs.items():
            if tab.get_widget() == widget:
                return tab

        return None

    def next_tab(self):
        # does not cycle
        #self.notebook.next_page()

        pos = self.notebook.get_current_page()
        num = self.notebook.get_n_pages()
        if num == 0:
            return
        pos = (pos + 1) % num
        self.notebook.set_current_page(pos)

    def prev_tab(self):
        # does not cycle
        #self.notebook.prev_page()

        pos = self.notebook.get_current_page()
        num = self.notebook.get_n_pages()
        if num == 0:
            return
        pos = (pos - 1) % num
        self.notebook.set_current_page(pos)

    def next_tab_action(self, widget, args):
        self.next_tab()
        return True

    def prev_tab_action(self, widget, args):
        self.prev_tab()
        return True

    def setup_style(self):
        self.bookmarks.set_name("bookmarks")

        style = """
        list#bookmarks row {
          margin-top: 4px;
          margin-left: 4px;
          margin-right: 4px;

          padding-left: 8px;
          padding-top: 4px;
          padding-bottom: 4px;
        }
        """

        provider = Gtk.CssProvider()
        provider.load_from_data(style, -1)
        priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), provider, priority)

    def get_show_tabs(self):
        return self.visible

    def set_show_tabs(self, visible):
        if visible:
            self.bookmarks.show()
        else:
            self.bookmarks.hide()
        self.visible = visible

    def get_active_tab_id(self):
        pos = self.notebook.get_current_page()
        wdgt = self.notebook.get_nth_page(pos)

        # hacky as fuck
        for tab in self.tabs:
            if self.tabs[tab].get_wdgt() == wdgt:
                return self.tabs[tab].ident

        return None


class Tab:

    def __init__(self, view, ident, name):
        self.view = view

        self.log = self.view.log.getChild(type(self).__name__)
        self.config = self.view.config
        self.operator = self.view.operator

        self.window = self.view.window

        self.ident = ident
        self.name = name

        self.label_box = None

        self.hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.hbox.set_homogeneous(True)
        self.hbox.props.vexpand = True

        self.searchsidebar = SearchSidebar(self.view, self)
        self.hbox.append(self.searchsidebar.get_wdgt())

        #print(self.config)

        if self.config["renderer"] == "webkit":
            self.viewer = labnote.webview.WebView(self.view, ident)
        if self.config["renderer"] == "gtk":
            self.viewer = labnote.gtkview.GtkView(self.view, ident)

        self.source = SourceView(self, self.view, ident)

        # TODO seperating editor and view
        # use frames?
        # separator uses backgroundcolor
        # frame uses borders?
        # test setting separator style class to frame
        separator = Gtk.Separator()
        style_context = separator.get_style_context()
        style_context.add_class("frame")
        separator.props.hexpand = False
        #separator.props.halign = Gtk.Align.END

        if self.config["editor-right"]:
            self.hbox.append(self.viewer.get_wdgt())
            #self.hbox.append(separator)
            self.hbox.append(self.source.get_wdgt())
        else:
            self.hbox.append(self.source.get_wdgt())
            #self.hbox.append(separator)
            self.hbox.append(self.viewer.get_wdgt())

        self.source.get_wdgt().props.hexpand = True

        # TODO
        #self.viewer.get_wdgt().connect("button-press-event", self.on_button_press)

        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.vbox.append(self.hbox)

        self.statusbar = Statusbar(self.view)
        self.vbox.append(self.statusbar.get_wdgt())

        self.searchbar_global = SearchBarGlobal(self)
        self.vbox.append(self.searchbar_global.get_wdgt())

        self.searchbar_local = SearchBarLocal(self)
        self.vbox.append(self.searchbar_local.get_wdgt())

        if self.config["adaptive"]:
            # TODO start in EDIT if no content
            self.show(ViewMode.VIEW)
        else:
            self.show()

        self.searchsidebar.hide()
        self.searchbar_global.hide()
        self.searchbar_local.hide()

    def get_wdgt(self):
        return self.vbox

    def get_callback(self):
        return self.viewer.callback

    def set_name(self, name):
        self.name = name
        self.dn = labnote.refs.shorten_ref(name)
        self.label.set_text(self.dn)
        self.label.set_tooltip_text(self.name)  # show full path in tooltip

    def get_label(self):
        if self.label_box:
            return self.label_box

        self.label_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        self.label = Gtk.Label()
        self.label.set_name("tab-label")

        self.label.set_xalign(0)
        self.label_box.append(self.label)

        self.label_button = Gtk.Button.new_from_icon_name("window-close")
        self.label_button.set_name("tab-button-close")
        #self.label_button.add_css_class("flat")

        sep = Gtk.Label.new(" ")
        sep.props.hexpand = True
        self.label_box.append(sep)
        self.label_box.append(self.label_button)

        self.label_button.connect("clicked", self.on_button_clicked)

        self.style_closebuttons()

        return self.label_box

    def show(self, mode=ViewMode.BOTH):
        # TODO
        if not isinstance(mode, ViewMode):
            raise Exception

        self.mode = mode

        if self.mode == ViewMode.BOTH:
            self.source.show()
            self.viewer.show()
        elif self.mode == ViewMode.VIEW:
            self.source.hide()
            self.viewer.show()
        elif self.mode == ViewMode.EDIT:
            self.source.show()
            self.viewer.hide()

    def on_button_clicked(self, button):
        self.operator.enqueue(labnote.events.CloseTab(self.ident))

    def style_closebuttons(self):

        style = """
        #tab-button-close {
          min-height: 0px;
          min-width: 0px;

          padding-top: 0px;
          padding-bottom: 0px;

          background-image: none;
          border-style: none;
        }
        """

        context = self.label_button.get_style_context()

        provider = Gtk.CssProvider()
        provider.load_from_data(style, -1)

        priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION

        Gtk.StyleContext.add_provider(context, provider, priority)


class SearchBarLocal:

    def __init__(self, tab):
        self.tab = tab
        self.ident = tab.ident

        self.hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.hbox.set_name("searchbar-local")

        self.entry = Gtk.Entry()
        self.entry.props.hexpand = True
        self.hbox.append(self.entry)

        self.entry.set_icon_from_icon_name(
            Gtk.EntryIconPosition.PRIMARY,
            "edit-find")
        self.entry.set_placeholder_text("search in current file")

        # TODO add buttons
        # - search (forward)
        # - backward
        # - close
        # options?

        self.button_next = Gtk.Button.new_from_icon_name("go-down")
        self.button_next.connect("clicked", self.on_button_next_clicked)
        self.hbox.append(self.button_next)

        self.button_close = Gtk.Button.new_from_icon_name("window-close")
        self.button_close.connect("clicked", self.on_button_close_clicked)
        self.hbox.append(self.button_close)

        self.entry.connect("activate", self.on_activated)
        self.entry.connect("changed", self.on_changed)

        ec_key = Gtk.EventControllerKey()
        ec_key.connect("key-pressed", self.on_key_pressed)
        self.hbox.add_controller(ec_key)

    def get_wdgt(self):
        return self.hbox

    def show(self):
        self.tab.view.log.debug("search local")
        self.tab.searchbar_global.hide()
        self.hbox.show()
        self.entry.grab_focus()

        self.on_changed(self.entry)

    def hide(self):
        self.hbox.hide()

        # only if hidden after search
        if self.entry.get_text():
            # clear search results shown
            self.search("")

    def search(self, term):
        # this will scroll to match
        self.tab.source.search(term)
        # now called by source
        #self.tab.viewer.search(term)

    def search_next(self):
        # this will scroll to match
        self.tab.source.search_next()
        # now called by source
        #self.tab.viewer.search_next()

    def on_changed(self, entry):
        term = entry.get_text()
        self.search(term)

    def on_activated(self, entry):
        self.search_next()

    def on_key_pressed(self, ec, keyval, keycode, state):
        if keyval == Gdk.KEY_Escape:
            self.hide()
            self.tab.view.operator.enqueue(labnote.events.FocusEditor())
            return True
        return False

    def on_button_next_clicked(self, button):
        self.search_next()

    def on_button_close_clicked(self, button):
        self.hide()


class SourceView:

    def __init__(self, tab, view, ident):
        self.view = view
        self.ident = ident
        self.tab = tab

        self.log = self.view.log.getChild(type(self).__name__)
        self.config = self.view.config
        self.operator = self.view.operator

        self.sourceview = GtkSource.View()

        ec_key = Gtk.EventControllerKey()
        ec_key.connect("key-pressed", self.on_key_pressed)
        self.sourceview.add_controller(ec_key)

        drop_target = Gtk.DropTarget.new(Gdk.FileList, Gdk.DragAction.LINK)
        drop_target.connect("accept", self.on_dnd_accept)
        drop_target.connect("enter", self.on_dnd_enter)
        drop_target.connect("drop", self.on_dnd_drop)
        self.sourceview.add_controller(drop_target)

        self.scrolled_window = Gtk.ScrolledWindow()
        self.scrolled_window.set_child(self.sourceview)

        self.buffer = self.sourceview.get_buffer()

        self.setup()
        if self.config["spellcheck"]:
            self.setup_spellcheck()
        self.setup_style()
        self.setup_highlighting()
        self.setup_whitespace()

        self.setup_search()

        self.setup_context_menu()

        self.clipman = ClipboardManager()

        self.buffer.connect("changed", self.on_buffer_changed)
        self.buffer.connect("modified-changed", self.on_modified_changed)

    def get_wdgt(self):
        return self.scrolled_window

    def show(self):
        self.scrolled_window.show()

    def hide(self):
        self.scrolled_window.hide()

    def setup_spellcheck(self):
        # libspelling -> enchant -> aspell/hunspell
        # ~/.config/enchant/de_DE.dic
        checker = Spelling.Checker.get_default()
        self.spelling_adapter = Spelling.TextBufferAdapter.new(self.buffer, checker)
        extra_menu = self.spelling_adapter.get_menu_model()

        self.sourceview.set_extra_menu(extra_menu)
        self.sourceview.insert_action_group("spelling", self.spelling_adapter)

    def setup(self):
        self.sourceview.set_tab_width(2)
        self.sourceview.set_insert_spaces_instead_of_tabs(True)
        self.sourceview.set_show_line_numbers(True)
        self.sourceview.set_right_margin_position(80)
        self.sourceview.set_show_right_margin(True)
        self.sourceview.set_highlight_current_line(True)
        #self.sourceview.set_wrap_mode(Gtk.WrapMode.NONE)
        # easier to compare text documents
        self.sourceview.set_wrap_mode(Gtk.WrapMode.WORD_CHAR)
        self.sourceview.set_auto_indent(False)
        self.sourceview.set_smart_home_end(True)

    def setup_style(self):
        self.sourceview.set_name("rstsource")

        font_desc = self.config["font-mono"]
        self.log.debug("using font %s", font_desc)
        if not font_desc:
            return

        font = " ".join(font_desc.split(" ")[:-1])
        size = int(font_desc.split(" ")[-1])

        style = f"""
        #rstsource {{
          font-family: {font}, monospace;
          font-size: {size}pt;
          /*
          padding-bottom: 50px;
          */
          margin: 1px;
        }}
        """
        provider = Gtk.CssProvider()
        provider.load_from_data(style, -1)

        context = self.sourceview.get_style_context()
        priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        Gtk.StyleContext.add_provider(context, provider, priority)

    def setup_highlighting(self):
        fp = self.operator.model.get_tab(self.ident)["file"]

        lm = GtkSource.LanguageManager.get_default()

        if fp.endswith(".rst"):
            self.buffer.props.language = lm.get_language("rst")
        elif fp.endswith(".md"):
            self.buffer.props.language = lm.get_language("markdown")
        else:
            return

        ssm = GtkSource.StyleSchemeManager.get_default()
        scheme = ssm.get_scheme(self.config["style-sourceview"])
        self.log.debug("GtkSource highlighting %s", scheme.get_filename())
        self.buffer.props.style_scheme = scheme

    def setup_whitespace(self):
        # show trailing whitespace (gtk >= 3.24)
        spacedrawer = self.sourceview.get_space_drawer()
        # seems to turn all on
        spacedrawer.set_enable_matrix(True)
        # all off
        spacedrawer.set_types_for_locations(
            GtkSource.SpaceLocationFlags.ALL,
            GtkSource.SpaceTypeFlags.NONE)
        # display all trailing whitespaces
        spacedrawer.set_types_for_locations(
            GtkSource.SpaceLocationFlags.TRAILING,
            GtkSource.SpaceTypeFlags.SPACE)

    def setup_search(self):
        self.searchsettings = GtkSource.SearchSettings()
        self.searchsettings.set_case_sensitive(False)
        self.searchsettings.set_wrap_around(True)
        self.searchcontext = GtkSource.SearchContext.new(self.buffer, self.searchsettings)
        # iterator on textbuf, has to be invalidated on buf switch
        self.search_pos = None

    def check_spelling(self):
        if not self.config["spellcheck"]:
            self.view.status_set_timeout("Spellcheck disabled (libspelling not installed)")
            return

        if self.spelling_adapter.get_enabled():
            self.spelling_adapter.set_enabled(False)
        else:
            self.spelling_adapter.set_enabled(True)

    def search(self, term):
        self.log.debug("search for: %s", term)
        self.viewer = self.view.window.tabs[self.ident].viewer

        self.searchsettings.set_search_text(term)
        it = self.buffer.get_start_iter()
        found, start, end, wrapped = self.searchcontext.forward(it)
        if not found:
            self.viewer.search("", 0)
            return

        self.sourceview.scroll_to_iter(start, 0.1, True, 0.5, 0.0)
        self.buffer.place_cursor(start)
        self.search_pos = end.get_line() + 1

        line = start.get_line()
        self.viewer.search(term, line)

    def search_next(self):
        self.log.debug("search continued")
        if not self.search_pos:
            it = self.buffer.get_start_iter()  # TODO
        else:
            _, it = self.buffer.get_iter_at_line(self.search_pos)

        self.log.debug("continue from line %s", it.get_line())

        found, start, end, wrapped = self.searchcontext.forward(it)
        if not found:
            self.viewer.search("", 0)
            return

        self.sourceview.scroll_to_iter(start, 0.1, True, 0.5, 0.0)
        self.buffer.place_cursor(start)
        self.search_pos = end.get_line() + 1

        line = start.get_line()
        self.viewer.search_next(line)

    def set_text(self, text, pos):
        # invalidate last search position
        self.search_pos = None

        # change buffer contents
        self.buffer.handler_block_by_func(self.on_buffer_changed)
        self.buffer.begin_irreversible_action()
        self.buffer.set_text(text)
        self.buffer.end_irreversible_action()
        self.buffer.set_modified(False)
        self.buffer.handler_unblock_by_func(self.on_buffer_changed)

        # set cursor position
        if pos >= 0:
            #self.set_pos(pos)
            # defer after text realization
            GLib.idle_add(self.set_pos, pos)

        # update
        #self.update()
        # defer after set text and cursor position
        GLib.idle_add(self.update)

    def get_text(self):
        return self.buffer.props.text

    def set_pos(self, pos):
        self.log.debug("set cursor at %s and scroll into view", pos)

        if pos < 0:
            return

        ret, it = self.buffer.get_iter_at_line(pos)
        self.buffer.place_cursor(it)

        # needs to be deferred after loading text
        mark = self.buffer.create_mark("scroll", it, True)
        margin = 0.0  # reduce by fraction of screen size?
        use_align = True
        xalign = 0.0
        #yalign = 0.5
        yalign = 0.3
        self.sourceview.scroll_to_mark(mark, margin, use_align, xalign, yalign)

    def get_pos(self):
        cursor_mark = self.buffer.get_insert()
        cursor_iter = self.buffer.get_iter_at_mark(cursor_mark)
        line = cursor_iter.get_line()
        if not line:
            line = 0
        self.log.debug("get pos {}".format(line))
        return line

    def update(self):
        if self.config["benchmark"]:
            self.view.time_start = time.process_time_ns()
            self.view.time_last = self.view.time_start
            print("buffer changed")

        if self.config["renderer"] == "webkit":
            cmd = labnote.events.UpdateWebView(self.ident, self.get_text(), self.get_pos())
            self.operator.enqueue(cmd)

        if self.config["renderer"] == "gtk":
            cmd = labnote.events.UpdateGtkView(self.ident, self.get_text(), self.get_pos())
            self.operator.enqueue(cmd)

    def on_buffer_changed(self, tvbuffer):
        self.log.debug("buffer changed " + threading.current_thread().name)
        if self.view.window.get_mode() == ViewMode.BOTH:
            self.update()

    def on_dnd_enter(self, target, x, y):
        return Gdk.DragAction.LINK

    def on_dnd_accept(self, target, drop):
        #print("dnd_accept")
        #print("actions", drop.get_actions())
        #print("formats", drop.get_formats().to_string())
        return True

    def on_dnd_drop(self, target, value, x, y):
        #print("dnd_drop")

        x, y = self.sourceview.window_to_buffer_coords(Gtk.TextWindowType.TEXT, x, y)
        over_text, it = self.sourceview.get_iter_at_location(x, y)

        basedir = self.operator.model.get_base()
        fp = self.operator.model.tabs[self.ident]["file"]
        curdir = os.path.dirname(fp)

        for gf in value.get_files():
            fp = gf.get_path()
            #print(fp)

            fp = labnote.refs.minify_fp2ref(fp, basedir, curdir)
            ref = labnote.refs.make_reference(fp)
            self.buffer.insert(it, ref)

        return True

    def on_key_pressed(self, ec, keyval, keycode, state):
        clipboard = Gdk.Display.get_default().get_clipboard()

        if state & Gdk.ModifierType.CONTROL_MASK:
            # Ctrl d
            if keyval == Gdk.KEY_d:
                # diable "delete line" for sourceview
                #return True

                # delete line (and copy to clipboard)
                # TODO handle selection? include newline?
                line = self.get_pos()

                found, start = self.buffer.get_iter_at_line(line)

                found, end = self.buffer.get_iter_at_line(line)
                # If the iterator is already at the paragraph delimiter characters,
                # moves to the paragraph delimiter characters for the next line.
                #end.forward_to_line_end()

                end.forward_line()
                #end.backward_char()

                # check if at line end
                #if not end.ends_line():
                #    end.forward_to_line_end()
                #    end.backward_char()

                text = self.buffer.get_text(start, end, False)
                clipboard.set(text)

                self.buffer.delete(start, end)

            # Ctrl Shift v
            if keyval == Gdk.KEY_V:
                # paste clipboard as literal block
                #clipboard.read_text_async(None, self.on_clipboard_read)
                self.clipman.get_text(self.insert_lb)
                return True

            # Ctrl v
            if keyval == Gdk.KEY_v:
                # check available data
                formats = self.clipman.clipboard.get_formats()
                types = formats.get_mime_types()

                for typ in types:
                    if typ.startswith("image/"):
                        #print("insert image")
                        self.clipman.get_image(self.insert_image)
                        return True

                    if "uri-list" in typ:
                        #print("insert uris")
                        self.clipman.get_filelist(self.insert_filelist)
                        return True

                #print("insert text")
                self.clipman.get_text(self.insert)
                return True

        return False

    def insert_lb(self, text):
        lb = labnote.refs.make_literal_block(text)
        self.insert(lb)

    def insert_filelist(self, action, files):
        text = ""

        dp_base = self.view.operator.model.get_base()
        tab = self.operator.model.get_tab(self.ident)
        fp_c = tab["file"]
        dp = os.path.dirname(fp_c)

        for fp in files:
            fp = labnote.refs.minify_fp2ref(fp, dp_base, dp)
            ref = labnote.refs.make_reference(fp)
            text += ref + "\n"

        if text:
            self.insert(text)

    # TODO offer creating thumbnail for copied files?
    # TODO offer to copy/move files for dnd?
    def insert_image(self, texture):
        dp_base = self.view.operator.model.get_base()
        tab = self.operator.model.get_tab(self.ident)
        fp_c = tab["file"]
        dp = os.path.dirname(fp_c)

        if dp:
            if not os.path.exists(dp):
                os.makedirs(dp, exist_ok=True)

        fd, fp = tempfile.mkstemp(prefix="clip_", suffix=".png", dir=dp)
        texture.save_to_png(fp)
        os.close(fd)

        # create thumbnail
        fp_tmb = fp.replace(".png", ".jpg")
        pb = GdkPixbuf.Pixbuf.new_from_file_at_scale(fp, 600, 600, True)
        # TODO use PIL for optimized compression?
        pb.savev(fp_tmb, "jpeg", ["quality"], ["80"])

        # add to git
        self.operator.model.git.add(fp)
        self.operator.model.git.add(fp_tmb)

        # add ref to editor
        fp = labnote.refs.minify_fp2ref(fp, dp_base, dp)
        fp_tmb = labnote.refs.minify_fp2ref(fp_tmb, dp_base, dp)
        ref = labnote.refs.make_reference(fp_tmb, fp)
        self.insert(ref + "\n")

    def insert(self, text):
        # insert w/ overwrite

        self.buffer.handler_block_by_func(self.on_buffer_changed)
        sel = self.buffer.get_selection_bounds()
        if sel:
            self.buffer.delete(sel[0], sel[1])
        self.buffer.handler_unblock_by_func(self.on_buffer_changed)

        self.buffer.insert_at_cursor(text)

        # select pasted text
        #mark = self.buffer.get_insert()
        #end = self.buffer.get_iter_at_mark(mark)
        #start = self.buffer.get_iter_at_mark(mark)
        #start.backward_chars(len(text))
        #self.buffer.select_range(start, end)

    def on_modified_changed(self, buffer):
        self.tab.statusbar.set_modified(buffer.get_modified())

    def setup_context_menu(self):
        model = self.sourceview.get_extra_menu()
        if not model:
            model = Gio.Menu()

        group = Gio.SimpleActionGroup()
        self.sourceview.insert_action_group("sourceview", group)

        action = Gio.SimpleAction.new("reformat", None)
        action.connect("activate", self.on_reformat_text)
        group.insert(action)
        item = Gio.MenuItem.new("Reformat Selection", "sourceview.reformat")
        model.insert_item(-1, item)

        action = Gio.SimpleAction.new("rewrap", None)
        action.connect("activate", self.on_rewrap_text)
        group.insert(action)
        item = Gio.MenuItem.new("Rewrap Selection", "sourceview.rewrap")
        model.insert_item(-1, item)

        action = Gio.SimpleAction.new("wc", None)
        action.connect("activate", self.on_count_words)
        group.insert(action)
        item = Gio.MenuItem.new("Count", "sourceview.wc")
        model.insert_item(-1, item)

        self.sourceview.set_extra_menu(model)

    def on_reformat_text(self, action, param):
        self.log.debug("reformat text")

        # get selection
        r = self.buffer.get_selection_bounds()
        if not r:
            return
        start, end = r
        txt = self.buffer.get_text(start, end, False)

        txt = labnote.utils.reformat_text(txt)

        self.insert(txt)

    def on_rewrap_text(self, action, param):
        self.log.debug("rewrap text")

        # get selection
        r = self.buffer.get_selection_bounds()
        if not r:
            return
        start, end = r
        txt = self.buffer.get_text(start, end, False)

        txt = labnote.utils.rewrap_text(txt)

        self.insert(txt)

    def on_count_words(self, action, param):

        info = "Count, overall"

        full = self.get_text()
        info += " Words {}".format( len(full.split(" ")) )
        info += " Chars {}".format( len(full) )

        r = self.buffer.get_selection_bounds()
        if r:
            info += ", selected"
            start, end = r
            selected = self.buffer.get_text(start, end, False)
            info += " Words {}".format( len(selected.split(" ")) )
            info += " Chars {}".format( len(selected) )

        self.tab.statusbar.set_timeout(info, 10)


class ClipboardManager:

    def __init__(self):
        self.clipboard = Gdk.Display.get_default().get_clipboard()

    def set_text(self, text):
        self.clipboard.set(text)

    def get_text(self, callback):
        self.cb_text = callback
        self.clipboard.read_text_async(None, self._got_text)

    def _got_text(self, clipboard, result):
        try:
            text = clipboard.read_text_finish(result)
        except GLib.GError:
            return

        self.cb_text(text)

    def set_image(self, texture):
        # TODO
        return

    def get_image(self, callback):
        self.cb_image = callback
        self.clipboard.read_texture_async(None, self._got_image)

    def _got_image(self, clipboard, result):
        try:
            texture = clipboard.read_texture_finish(result)
        except GLib.GError:
            return

        self.cb_image(texture)

    def set_filelist(self, files, action="copy"):
        urls = [action]
        for f in files:
            url = "file://"
            url += urllib.parse.quote(os.path.abspath(f))
            urls.append(url)

        data = "\n".join(urls).encode()
        gbytes = GLib.Bytes.new(data)

        content = Gdk.ContentProvider.new_for_bytes("x-special/gnome-copied-files", gbytes)
        self.clipboard.set_content(content)


    def get_filelist(self, callback):
        self.cb_filelist = callback

        formats = self.clipboard.get_formats()
        mime_types = formats.get_mime_types()

        if "x-special/gnome-copied-files" in mime_types:
            self.expected = "gnome-copied-files"
            self.clipboard.read_async(
                ["x-special/gnome-copied-files"],
                GLib.PRIORITY_DEFAULT,
                None,
                self._got_filelist_stream
            )
            return

        if "text/uri-list" in mime_types:
            self.expected = "uri-list"
            self.clipboard.read_async(
                ["text/uri-list"],
                GLib.PRIORITY_DEFAULT,
                None,
                self._got_filelist_stream
            )
            return


    def _got_filelist_stream(self, clipboard, result):
        try:
            stream, mime = clipboard.read_finish(result)
        except GLib.GError:
            return

        #self.readsize = 10  # test
        self.readsize = 4096
        self.buffer = b""
        stream.read_bytes_async(self.readsize, GLib.PRIORITY_DEFAULT, None, self._get_filelist_data)

    def _get_filelist_data(self, stream, result):
        gbytes = stream.read_bytes_finish(result)
        data = gbytes.get_data()

        self.buffer += data

        if len(gbytes.get_data()) != self.readsize:
            self._got_filelist(self.buffer)
            return

        stream.read_bytes_async(self.readsize, GLib.PRIORITY_DEFAULT, None, self._get_filelist_data)

    def _got_filelist(self, data):
        data = data.decode()  # assume utf8
        data = data.split("\n")

        if self.expected == "gnome-copied-files":
            action = data[0]
            data = data[1:]
        else:
            action = "copy"

        files = []
        for url in data:
            #res = urllib.parse.urlparse(url)
            #fp = os.path.join(res.netloc, res.path)
            fp = urllib.parse.unquote(url[7:])
            files.append(fp)

        self.cb_filelist(action, files)



