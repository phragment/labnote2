# This file is part of LabNote2
# Copyright 2019-2021 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import collections
import gc
import threading

import labnote.model
import labnote.view


class Operator:

    instance = None

    def ineedanexit():
        return Operator.instance

    def __init__(self, log, config):
        self.log = log.getChild(type(self).__name__)
        self.config = config

        self.queue = Queue(self.log)
        self.running = True

        self.view = labnote.view.View(log, config, self)
        self.model = labnote.model.Model(log, config, self.view)

        self.storage = {}

        Operator.instance = self

    def run(self):
        #gc.disable()

        op_thread = threading.Thread(target=self.process, name="OpThread")
        op_thread.start()

        # Gtk has to be run from thread which created the objects
        self.view.run()

        op_thread.join()

        self.log.info("shutdown finished")

    def process(self):
        self.log.debug("starting in " + threading.current_thread().name)
        while self.running:
            event = self.queue.get()

            if not isinstance(event, labnote.events.Event):
                self.log.warning("unknown event: " + str(event))
                continue

            # NOTE this runs while new events are added!
            # therefore it is possible that the loop does not finish,
            # in case new update events are queued while running

            # skip intermediate updates if they queue up
            if isinstance(event, labnote.events.Update):
                e = self.queue.peek()
                while isinstance(e, labnote.events.Update):
                    e = self.queue.peek()
                    event = self.queue.get()

            self.cur = event
            event.process(self)
            self.cur = None

            #gb = gc.collect()
            #self.log.debug("gc collected %s objects", gb)

    def stop(self):
        self.running = False

    def enqueue(self, cmd):
        #self.log.debug("exec " + str(cmd))
        self.queue.put(cmd)


class Queue:

    def __init__(self, log):
        self.log = log

        # thread-safe and memory efficient queue providing
        # fast appends and pops from either side
        self.queue = collections.deque()

        # used to let the consumer sleep, NOT for locking
        self.sem = threading.Semaphore(value=0)

    def put(self, cmd):
        self.log.debug("put " + str(cmd))
        self.queue.append(cmd)
        self.sem.release()

    def get(self):
        self.sem.acquire()
        return self.queue.popleft()

    def peek(self):
        try:
            return self.queue[0]
        except IndexError:
            return None

