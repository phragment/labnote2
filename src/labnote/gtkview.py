# This file is part of LabNote2
# Copyright 2022-2024 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import cProfile
import os
import threading
import time
import traceback
import urllib

import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, Gdk, GLib, Gio, GObject

import labnote.gtkdtree
import labnote.events

import cProfile
import pstats


"""
TODO

ctrl left click to open in (background) tab

right click context menu open in (foregound) tab

auto scroll (mostly ok)

  search (via sourceview by line, mark found line?)

  TODO check scroll to line after global search

    in theory ok, but may need to be deferred further after realizing widgets


disable kinetic scrolling for desktop, enable for adaptive?

disable overlay scrolling for desktop, enable for adaptive?


add gallery


"""

class GtkView:

    def __init__(self, view, ident):
        self.view = view
        self.ident = ident

        self.log = self.view.log.getChild(type(self).__name__)

        self.sw = Gtk.ScrolledWindow()
        #self.sw.props.vadjustment.connect("value-changed", self.on_scroll_changed)

        self.list_view = Gtk.ListView()
        self.list_view.set_name("viewer")

        ec_lc = Gtk.GestureClick()
        ec_lc.props.button = 1
        ec_lc.connect("pressed", self.on_left_click)
        self.list_view.add_controller(ec_lc)

        ec_mc = Gtk.GestureClick()
        ec_mc.props.button = 2
        ec_mc.connect("pressed", self.on_middle_click)
        self.list_view.add_controller(ec_mc)

        self.list_view.add_controller(self.setup_context_menu())

        ec_lp = Gtk.GestureLongPress()
        ec_lp.set_propagation_phase(Gtk.PropagationPhase.CAPTURE)
        ec_lp.connect("pressed", self.on_long_press)
        self.list_view.add_controller(ec_lp)

        self.sw.set_child(self.list_view)

        self.factory = Gtk.SignalListItemFactory()
        self.factory.connect("setup", self.on_factory_setup)
        self.factory.connect("bind", self.on_factory_bind)
        self.list_view.set_factory(self.factory)

        # use selection as scroll marker
        #self.selection = Gtk.NoSelection()
        self.selection = Gtk.SingleSelection()

        self.model = Gio.ListStore()

        self.selection.set_model(self.model)
        self.list_view.set_model(self.selection)

        self.load_style()

    def get_wdgt(self):
        return self.sw

    def show(self):
        self.hidden = False
        self.sw.set_visible(True)

    def hide(self):
        self.hidden = True
        self.sw.set_visible(False)

    def stop(self):
        # exists as compatibility for shutting down webview
        pass

    def load_style(self):
        fp = self.view.config["style-gtkview"]
        self.log.debug("loading css from %s", fp)

        try:
            f = open(fp, "r")
        except FileNotFoundError:
            self.log.warning("could not load: %s", fp)
            return
        style = f.read()
        f.close()

        provider = Gtk.CssProvider()
        provider.connect("parsing-error", self.on_parsing_error)

        try:
            provider.load_from_string(style)
        except GLib.GError:
            self.log.debug("could not load gtk css: %s", fp)
            return

        priority = Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        Gtk.StyleContext.add_provider_for_display(Gdk.Display.get_default(), provider, priority)
        self.cssprov = provider

    def on_parsing_error(self, provider, section, error):
        #section.get_start_line()
        #section.get_section_type()
        self.view.log.error("could not parse gtk css: %s", error)

    # for switching between files
    # TODO is this used?
    def clear(self):
        self.log.debug("clearing list model")
        self.model.remove_all()

        self.sw.props.vadjustment.value = 0.0

    def update(self, entries, line=-1):
        self.update_naive(entries, line)
        #cProfile.runctx('self.update_naive(entries, line)', globals(), locals())

    def update_naive(self, entries, line=-1):
        # diff model entries
        # using comparison function for entry class
        # mostly naive implementation, O(m * n)

        # save sw position
        adj = self.sw.props.vadjustment
        last_scroll = adj.props.value

        # save selected pos
        last_sel = self.selection.props.selected

        if self.view.config["benchmark"]:
            ts = time.process_time_ns()
            dt = (ts - self.view.time_last) / 1000000
            dt_abs = (ts - self.view.time_start) / 1000000
            self.view.time_last = ts
            print("diff start", dt, dt_abs, "ms")

        # save current entries
        # O(n)
        entries_old = []
        for i in range(self.model.get_n_items()):
            entry = self.model.get_item(i)
            entries_old.append(entry)

        # remove entries which are not in the list anymore
        # O(n * m)
        rm = 0
        for i, entry in enumerate(entries_old):
            if entry not in entries:
                self.model.remove(i - rm)  # removing entries changes indices!
                rm += 1

        if self.view.config["benchmark"]:
            print(f"removed {rm}")

        # add new entries
        # O(n * m)
        n = 0
        for i, entry in enumerate(entries):
            if not entry in entries_old:
                # FIXME crashed with assertion is gobject failed, how?
                #if not entry:
                #    self.log.error("entry is None?")
                #if not isinstance(entry, GObject.Object):
                #    self.log.error("entry is nop gobject? %s", entry)

                self.model.insert(i, entry)
                n += 1

        if self.view.config["benchmark"]:
            print(f"added {n}")

        # update pass
        # O(n)
        for i, entry in enumerate(entries):
            entry_old = self.model.get_item(i)
            # is there a race condition?
            if not entry_old:
                print("TODO should not happen")
                continue
            entry_old.update(entry)

        if self.view.config["benchmark"]:
            ts = time.process_time_ns()
            dt = (ts - self.view.time_last) / 1000000
            dt_abs = (ts - self.view.time_start) / 1000000
            print("update done", dt, dt_abs, "ms")
            print("")

        # restore selection
        # seems to move if selected entry is updated (removed, readded)
        self.selection.props.selected = last_sel

        if self.view.config["scroll"]:
            if line >= -1:
                self.scroll_to_line(line)

    def update_sets(self, entries, line=-1):
        # diff model entries
        # using comparison function for entry class

        # save sw position
        adj = self.sw.props.vadjustment
        last_scroll = adj.props.value

        # save selected pos
        last_sel = self.selection.props.selected

        if self.view.config["benchmark"]:
            ts = time.process_time_ns()
            dt = (ts - self.view.time_last) / 1000000
            dt_abs = (ts - self.view.time_start) / 1000000
            self.view.time_last = ts
            print("diff start", dt, dt_abs, "ms")

        # this should have O(n + m) with large c?
        # defining n as the longer list -> O(n)
        entries_old = []
        for i in range(self.model.get_n_items()):
            entry = self.model.get_item(i)
            entries_old.append( (i, entry) )

        entries_new = []
        for i, entry in enumerate(entries):
            entries_new.append( (i, entry) )

        entries_old_set = set(entries_old)
        entries_new_set = set(entries_new)

        added = entries_new_set - entries_old_set
        removed = entries_old_set - entries_new_set
        #unchanged = entries_new_set & entries_old_set

        if self.view.config["benchmark"]:
            print(f"removed {len(removed)}")
            print(f"added {len(added)}")

        offset = 0
        for i, entry in removed:
            i = i - offset
            if i < 0:
                i = 0
            self.model.remove(i)
            offset += 1

        # python uses timsort (mergesort) increasing to O(n log n)
        # this has to be sorted, otherwise the indices could be non existant
        for i, entry in sorted(added):
            #print(entry)
            self.model.insert(i, entry)

        # update pass
        # O(n)
        for i, entry in enumerate(entries):
            entry_old = self.model.get_item(i)
            # is there a race condition?
            if not entry_old:
                print("TODO should not happen")
                continue
            entry_old.update(entry)

        if self.view.config["benchmark"]:
            ts = time.process_time_ns()
            dt = (ts - self.view.time_last) / 1000000
            dt_abs = (ts - self.view.time_start) / 1000000
            print("update done", dt, dt_abs, "ms")
            print("")

        # restore selection
        # seems to move if selected entry is updated (removed, readded)
        self.selection.props.selected = last_sel

        if self.view.config["scroll"]:
            if line >= -1:
                self.scroll_to_line(line)

    def on_factory_setup(self, factory, listitem):
        entry_widget = labnote.gtkdtree.EntryWidget(
            self.on_activate_link,
            self.on_activate_link_tab
        )
        listitem.set_child(entry_widget.get_widget())

    def on_factory_bind(self, factory, listitem):
        #pos = listitem.get_position()
        widget = listitem.get_child()
        entry = listitem.get_item()
        widget._entrywidget.update(entry)

    def on_list_view_activate(self, listview, pos):
        listitem = self.model.get_item(pos)
        lineno = listitem.lineno
        self.log.debug("scroll source view to %s", lineno)

        if lineno:
            source = self.view.window.tabs[self.ident].source
            source.set_pos(lineno - 1)

    def setup_context_menu(self):
        ec_gc = Gtk.GestureClick()
        ec_gc.set_propagation_phase(Gtk.PropagationPhase.CAPTURE)
        ec_gc.props.button = 3  # right click
        ec_gc.connect("pressed", self.show_context_menu)
        return ec_gc

    def show_context_menu(self, ec, press, x, y):
        # get widget by xy
        wdgt = self.list_view.pick(x, y, Gtk.PickFlags.DEFAULT)

        entrywidget = self._lookup_entrywidget(wdgt)
        if not entrywidget:
            return True

        uri = entrywidget.label.get_current_uri()
        if uri:
            entrywidget._uri = uri
            entrywidget.popover.popup()

        return True

    def on_activate_link_tab(self, button):
        ref = button._entrywidget._uri
        fp = labnote.refs.convert_laburi2fp(ref)
        self.view.operator.enqueue(labnote.events.OpenTab(fp))
        # hide popover
        button._entrywidget.popover.popdown()

    def on_activate_link(self, label, ref):
        # click on link in label
        self.log.debug("activate link %s", ref)

        fp = labnote.refs.convert_laburi2fp(ref)
        self.log.debug("activate link %s", fp)

        """
        if fp.startswith("#"):
            id_ = fp[1:]
            wdgt = self.id2wdgt[id_]
            self.scroll_to_widget(wdgt)

            # scroll editor
            #line = self.line_from_widget(wdgt)
            line = wdgt._line
            if line:
                tab = self.view.window.tabs[self.ident]
                tab.source.set_pos(line)

            return
        """

        if "://" in fp:
            self.view.operator.enqueue(labnote.events.OpenExternal(fp))
        else:
            if not fp.endswith(".rst") and not fp.endswith(".md"):
                self.view.operator.enqueue(labnote.events.OpenExternal(fp))
            else:
                self.view.operator.enqueue(labnote.events.Open(self.ident, fp))

        return True

    def on_left_click(self, ec, presses, x, y):
        # handles clicks on images?

        if not presses == 1:
            return False

        wdgt = self.list_view.pick(x, y, Gtk.PickFlags.DEFAULT)
        entry = self._lookup_entry(wdgt)
        if not entry:
            return False

        ref = entry.fp_ref
        if not ref:
            return False

        fp = labnote.refs.convert_laburi2fp(ref)
        if "://" in fp:
            self.view.operator.enqueue(labnote.events.OpenExternal(fp))
        else:
            if not fp.endswith(".rst") and not fp.endswith(".md"):
                self.view.operator.enqueue(labnote.events.OpenExternal(fp))
            else:
                self.view.operator.enqueue(labnote.events.Open(self.ident, fp))

        return True

    def on_middle_click(self, ec, presses, x, y):
        if not presses == 1:
            return False

        wdgt = self.list_view.pick(x, y, Gtk.PickFlags.DEFAULT)
        entry = self._lookup_entry(wdgt)
        if not entry:
            return False

        lineno = entry.lineno
        self.log.debug("scroll source view to %s", lineno)

        if lineno:
            source = self.view.window.tabs[self.ident].source
            source.set_pos(lineno - 1)

        return True

    def on_long_press(self, ec, x, y):
        self.log.debug("long press, show context menu")

        #wdgt = self.list_view.pick(x, y, Gtk.PickFlags.DEFAULT)
        #entry = self._lookup_entry(wdgt)
        #if not entry:
        #    return False

        self.show_context_menu(None, 0, x, y)

        return True

    def _lookup_entry(self, widget):
        while not isinstance(widget, Gtk.Box):
            if not widget:
                return None
            widget = widget.get_parent()
        try:
            return widget._entry
        except AttributeError:
            return None

    def _lookup_entrywidget(self, widget):
        while not isinstance(widget, Gtk.Box):
            if not widget:
                return None
            widget = widget.get_parent()
        try:
            return widget._entrywidget
        except AttributeError:
            return None

    def search(self, term, line):
        # called by sourceview
        # highlight entry?
        self.scroll_to_line(line)

    def search_next(self, line):
        self.search("", line)

    def on_scroll_changed(self, adj):
        self.log.debug("scrolling %s", adj.props.value)
        #adj.props.value = 0.0
        #return True

    def scroll_to(self, pos):
        GLib.idle_add(self._scroll, pos)

    def _scroll(self, pos):
        # wait for new widgets to be drawn
        Gtk.test_widget_wait_for_draw(self.list_view)

        # Gtk.ListScrollFlags NONE FOCUS SELECT
        self.list_view.scroll_to(pos, Gtk.ListScrollFlags.SELECT, None)

    def scroll_to_line(self, target):
        # TODO
        self.log.debug("searching for line %s", target)

        # find entry (dtree node) containing the line
        target += 1  # dtree starts at 1, textbuf at 0

        # search through model entries
        n = self.model.get_n_items()
        last = 0
        for i in range(n):
            entry = self.model.get_item(i)
            lineno = entry.lineno
            #print(target, lineno, i, last, entry.name)

            if not lineno or lineno < 0:
                #last = i  # ???
                continue

            if lineno == target:
                self.log.debug("found entry for exact line %s", entry.lineno)
                #self.list_view.scroll_to(i, Gtk.ListScrollFlags.SELECT, None)
                self.scroll_to(i)
                return

            if lineno >= target:
                self.log.debug("found entry after line %s", entry.lineno)
                self.scroll_to(last)
                return

            last = i

        self.log.debug("not found")

        # scrolling since gtk 4.12
        # https://docs.gtk.org/gtk4/method.ListView.scroll_to.html

    def scroll_to_widget(self, wdgt):
        self.log.debug("scrolling to %s", wdgt)
        # TODO test with gtk4
        # scroll only if not visible
        # if scrolling up, get top of widget in view
        # if scrolling down, get bottom of widget in view
        if not wdgt:
            return
        vadj = self.sw.get_vadjustment()
        dst = wdgt.translate_coordinates(self.sw, 0, vadj.get_value())
        if not dst:
            return
        wtop = dst[1]
        rect, base = wdgt.get_allocated_size()
        wbot = wtop + rect.height
        vadj.clamp_page(wtop, wbot)


    def clear_cache(self):
        # - reload gtk css
        # - clear image cache?
        # - update from buffer
        # - (reread rst file)
        self.log.debug("forced reload")

        ## reload gtk css
        Gtk.StyleContext.remove_provider_for_display(Gdk.Display.get_default(), self.cssprov)
        self.load_style()

        #return

        ## clear cache
        #model = self.view.operator.model
        #model.tabs[self.ident]["wf"].img_cache.cache = {}

        ## evict all widgets, then update
        self.model.remove_all()
        self.view.window.get_active_tab().source.update()


