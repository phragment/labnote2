# This file is part of LabNote2
# Copyright 2019-2021 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import re
import subprocess


def run(cmd, stdin=None, cwd=None, timeout=None):
    # blocking!
    try:
        proc = subprocess.Popen(
            cmd,
            cwd=cwd,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
    except FileNotFoundError:
        return -1, "file not found: " + cmd[0]

    try:
        out, err = proc.communicate(input=stdin, timeout=timeout)
    except subprocess.TimeoutExpired:
        proc.kill()
        out, err = proc.communicate()

    ret = out.decode(errors="replace").strip()

    if not ret:
        ret = err.decode().strip()

    return proc.returncode, ret


def start(cmd, cwd=None):
    # non-blocking
    # does not kill if parent terminates
    try:
        proc = subprocess.Popen(
            cmd,
            cwd=cwd,
            start_new_session=True
        )
    except FileNotFoundError:
        return -1, "file not found: " + cmd[0]


def strip_lines(txt):
    return "\n".join(l.strip() for l in txt.split("\n"))


def replace_nonascii(s):
    lookup = {
        "“": "\"",
        "”": "\"",
        "„": "\"",
        "’": "'",
        "‘": "'",
        "…": "...",
        "¬": "",
        "→": "->",
        "–": "-"  # -- 
    }
    return "".join([c if c not in lookup else lookup[c] for c in s])


def strip_newlines(txt):
    #return re.sub("\n{3,}", "\n\n", txt)
    return re.sub("\n{4,}", "\n\n\n", txt)


def remove_special(s):
    lookup = [
        "\u200b"  # zero width space
    ]
    return "".join([c if c not in lookup else "" for c in s])


def reformat_text(txt):
    """
    - rewrap
      - remove newlines
        if they dont end with punctuation?
        -> difficult to detect
    - convert non-ascii
    - add newlines after punctuation
      problem for links
      only if followed by space?
      still a problem for i.e. vs. etc.

      only if punctuation is followed by space and big letter?

    - remove multiple newlines >2 consequtive
    - remove leading and trailing whitespace
    """

    #txt = strip_lines(txt)
    txt = remove_special(txt)
    txt = replace_nonascii(txt)
    #txt = add_newlines(txt)
    txt = strip_lines(txt)
    txt = strip_newlines(txt)

    return txt


def rewrap_text(txt):
    txt = txt.replace(". ", ".\n")
    return txt



















