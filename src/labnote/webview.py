# This file is part of LabNote2
# Copyright 2022-2024 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import time

import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib, Gio, Gdk

try:
    gi.require_version("WebKit", "6.0")
    from gi.repository import WebKit
except Exception:
    pass

import labnote.transform
import labnote.events


class WebView:

    def __init__(self, view, ident):
        self.view = view
        self.ident = ident

        self.log = self.view.log.getChild(type(self).__name__)
        self.config = self.view.config
        self.operator = self.view.operator

        #self.context = WebKit.WebContext()
        #self.webview = WebKit.WebView.new_with_context(self.context)

        self.webview = WebKit.WebView()
        self.context = self.webview.get_context()

        self.context.set_cache_model(WebKit.CacheModel.DOCUMENT_BROWSER)
        # FIXME Cannot register URI scheme labnote more than once
        #self.context.register_uri_scheme("labnote", self.uri_scheme_labnote)
        self.scheme = f"labnote-{self.ident}"
        self.context.register_uri_scheme(self.scheme, self.uri_scheme_labnote)

        for scheme, cb in self.view.schemes.items():
            self.context.register_uri_scheme(scheme, self.uri_scheme_shim, cb)

        #deny_schemes = ["http", "https", "ftp", "ftps", "mailto"]
        #for scheme in deny_schemes:
        #    self.context.register_uri_scheme(scheme, self.uri_scheme_deny)

        sm = self.context.get_security_manager()
        #sm.register_uri_scheme_as_local("labnote")
        sm.register_uri_scheme_as_local(self.scheme)
        #sm.register_uri_scheme_as_no_access("labnote")

        self.box = Gtk.Box()
        self.webview.props.vexpand = True
        self.webview.props.hexpand = True
        self.box.append(self.webview)

        self.webview.connect("load-changed", self.on_load_changed)
        self.webview.connect("load-failed", self.on_load_failed)
        self.webview.connect("decide-policy", self.on_decide_policy)

        self.webview.connect("context-menu", self.on_context_menu)

        settings = self.webview.get_settings()
        # disable caching html
        settings.set_enable_page_cache(False)

        settings.set_enable_javascript(True)
        #settings.set_enable_javascript(False)

        settings.set_allow_file_access_from_file_urls(True)
        settings.set_allow_universal_access_from_file_urls(True)

        # no <a ping attibute
        settings.set_enable_hyperlink_auditing(False)

        #settings.set_enable_plugins(False)
        #settings.set_enable_java(False)
        #settings.set_enable_webaudio(False)
        #settings.set_enable_webgl(False)
        #settings.set_enable_media_stream(False)
        #settings.set_enable_mediasource(False)

        # TODO test
        #settings.set_enable_tabs_to_links(False)
        #settings.set_enable_spatial_navigation(True)

        self.setup_style()

        # allow for transparent background (check style css)
        self.webview.set_background_color(Gdk.RGBA(0, 0, 0, 0.0))

        self.state = -1
        self.scroll_pos = -1

    def get_wdgt(self):
        #return self.webview
        return self.box

    def stop(self):
        self.webview.try_close()

    def setup_style(self):
        font_desc = self.config["font-sans"]
        if not font_desc:
            return

        self.log.debug("using font %s", font_desc)
        font = " ".join(font_desc.split(" ")[:-1])
        size = int(font_desc.split(" ")[-1])
        # size in px not pt!
        size = int(size * 1.33)

        settings = self.webview.get_settings()
        settings.set_default_font_family(font)
        settings.set_default_font_size(size)
        settings.set_minimum_font_size(size - 2)

    #def cbtest(self, webview, res):
    #    r = webview.run_javascript_finish(res)
    #    pos = r.get_js_value().to_string()
    #    print(pos)

    def clear(self):
        pass

    def load_html(self, html):
        #js = "document.documentElement.scrollTop"
        #self.webview.run_javascript(js, None, self.cbtest)

        title = self.webview.get_title()  # bug? returns <string> if not set
        #print("got title", title)

        try:
            pos = int(title)
            if pos > 0:
                self.scroll_pos = pos
        except ValueError:
            pos = self.scroll_pos
        except TypeError:
            pos = self.scroll_pos

        html = labnote.transform.html_prep2(html, pos)

        self.webview.load_html(html, f"{self.scheme}://")

    def on_load_failed(self, webview, loadevent, uri, error):
        self.log.debug("load failed")

    def on_load_changed(self, webview, loadevent):
        if loadevent == WebKit.LoadEvent.STARTED:
            self.state = 0
        if loadevent == WebKit.LoadEvent.COMMITTED:
            self.state = 1
        if loadevent == WebKit.LoadEvent.FINISHED:
            self.log.debug("load done")
            self.state = 2

            if self.view.config["benchmark"]:
                ts = time.process_time_ns()
                dt = (ts - self.view.time_last) / 1000000
                dt_abs = (ts - self.view.time_start) / 1000000
                print("update done", dt, dt_abs, "ms")
                print("")

    def on_decide_policy(self, webview, decision, decision_type):
        if decision_type == WebKit.PolicyDecisionType.NAVIGATION_ACTION:
            nav = decision.get_navigation_action()
            req = nav.get_request()
            uri = req.get_uri()
            self.log.debug("navigation policy for %s", uri)

            scheme = uri.split("://")[0]
            if scheme in self.view.schemes.keys():
                return False

            if not uri.startswith("labnote"):
                decision.ignore()

                # open http/mailto/... externally (uri scheme other than labnote)
                self.operator.enqueue(labnote.events.OpenExternal(uri))

                return True  # handling done

        #if decision_type == WebKit.PolicyDecisionType.RESPONSE:
        #    req = decision.get_request()
        #    uri = req.get_uri()
        #    self.log.debug("deciding policy for response: " + uri)

        return False  # not handled

    def uri_scheme_deny(self, request):
        self.log.debug("uri scheme denied %s", request.get_uri())

    def uri_scheme_labnote(self, request):
        uri = request.get_uri()
        self.log.debug("uri %s", uri)
        fp = labnote.refs.convert_laburi2fp(uri, self.scheme)

        if self.state == 0:
            if fp.endswith(".rst") or fp.endswith(".md"):
                # TODO file ending in rst and within base dir
                self.operator.enqueue(labnote.events.Open(self.ident, fp))
            else:
                self.operator.enqueue(labnote.events.OpenExternal(fp))

                # "finish" the load so the view is unblocked!
                self.webview.stop_loading()

        if self.state == 1:
            self.load_img(request)

    def uri_scheme_shim(self, request, cb):
        uri = request.get_uri()
        cb(uri)
        self.webview.stop_loading()

    def load_img(self, request):
        uri = request.get_uri()
        fp = labnote.refs.convert_laburi2fp(uri, self.scheme)

        self.log.debug("load file %s", fp)
        try:
            stream = Gio.file_new_for_path(fp).read()
        except GLib.Error:
            stream = Gio.MemoryInputStream.new_from_data(
                "file not found".encode())
            self.log.debug("file not found")
        request.finish(stream, -1, None)

    def hide(self):
        self.box.hide()

    def show(self):
        self.box.show()

    def search(self, term, line):
        self.find_controller = self.webview.get_find_controller()
        opt = WebKit.FindOptions.CASE_INSENSITIVE
        opt |= WebKit.FindOptions.WRAP_AROUND
        self.find_controller.search(term, opt, 2**16)

    def search_next(self, line):
        self.find_controller.search_next()

    def clear_cache(self):
        # FIXME
        #self.context.clear_cache()
        pass

    def on_context_menu(self, webview, context_menu, hittestresult):
        uri = hittestresult.get_link_uri()
        if not uri:
            return True

        context_menu.remove_all()
        action = Gio.SimpleAction.new("opentab", None)
        action.connect("activate", self.on_open_tab, uri)
        open_tab = WebKit.ContextMenuItem.new_from_gaction(action, "Open in New Tab")
        context_menu.prepend(open_tab)
        return False

    def on_open_tab(self, action, parameter, uri):
        fp = labnote.refs.convert_laburi2fp(uri, self.scheme)
        self.operator.enqueue(labnote.events.OpenTab(fp))

    def on_close_tab(self, action):
        self.operator.enqueue(labnote.events.CloseTab(self.ident))

    def scroll_to_line(self, line):
        self.log.debug("todo")

    def get_pos(self):
        self.log.debug("todo")
        return 0




