# This file is part of LabNote2
# Copyright 2019-2024 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import datetime
import os
import shutil
import subprocess
import sys
import tempfile
import time
#import gc

import labnote.transform
import labnote.refs


class Event:

    def process(self, operator):
        raise NotImplementedError("no process function")


class Quit(Event):

    def process(self, operator):
        operator.log.debug("shutting down")

        # check if any buffer modified
        view = operator.model.view
        for ident, tab in operator.model.tabs.items():
            if view.buffer_is_modified(ident):
                view.info_ask("Discard changes?",
                              [labnote.events.Unmodify(ident),
                               labnote.events.Quit()])
                return

        operator.model.git.commit()
        # wait some time on dialog for ssh key passwords
        operator.view.status_set("waiting 30s for git push")
        operator.model.git.push(timeout=30)

        # delete cache
        cachedir = "/tmp/labnote.cache"
        if os.path.exists(cachedir):
            shutil.rmtree("/tmp/labnote.cache")

        operator.view.stop()
        operator.stop()


class CloseTab(Event):

    def __init__(self, ident):
        self.ident = ident

    def process(self, operator):
        operator.log.debug("closing tab " + str(self.ident))
        # all tabs can be closed
        operator.model.git.commit()
        operator.model.close_tab(self.ident)


class OpenTab(Event):

    def __init__(self, fp, line=0):
        self.fp = fp
        self.line = line

    def process(self, operator):
        operator.log.debug("opening new tab " + self.fp)

        # TODO share code with view / uri_scheme_labnote ?

        # only open in tab if
        # - path points to a file (problem if not existant)
        # - filename ends with .rst
        # - (abs) path is within cwd/base

        #if os.path.isfile(self.fp):
        if True:
            if self.fp.endswith(".rst") or self.fp.endswith(".md"):
                if not os.path.isabs(self.fp):
                    operator.model.open_tab(self.fp, self.line)
                    return
                else:
                    base = operator.model.get_base()
                    if os.path.commonpath([self.fp, base]) == base:
                        # TODO ensure model.open_tab is called with a rel fp?

                        fp = os.path.relpath(self.fp, base)

                        operator.model.open_tab(fp, self.line)
                        return

        operator.enqueue(OpenExternal(self.fp))


class Update(Event):

    def process(self, operator):
        raise NotImplementedError("no process function")


class UpdateWebView(Update):

    def __init__(self, ident, text, line=0):
        self.ident = ident
        self.text = text
        self.line = line

    def process(self, operator):
        operator.log.debug("updating #{} line {}".format(self.ident, self.line))

        if operator.view.config["benchmark"]:
            ts = time.process_time_ns()
            dt = (ts - operator.view.time_last) / 1000000
            operator.view.time_last = ts
            print("events.updatewebkit", dt, "ms")

        fp = operator.model.tabs[self.ident]["file"]
        cd = os.path.dirname(fp)
        try:
            if fp.endswith(".rst"):
                html = labnote.transform.rst2html(self.text, cd, operator.config, self.line, self.ident)
            if fp.endswith(".md"):
                html = labnote.transform.md2html(self.text, cd, operator.config, self.line, self.ident)
        except Exception as e:
            operator.log.warning("error while generating html", exc_info=e)
            return

        if operator.view.config["benchmark"]:
            ts = time.process_time_ns()
            dt = (ts - operator.view.time_last) / 1000000
            operator.view.time_last = ts
            print("rst2html", dt, "ms")

        operator.model.load_html(self.ident, html)


class UpdateGtkView(Update):

    def __init__(self, ident, text, line=0):
        self.ident = ident
        self.text = text
        self.line = line

    def process(self, operator):
        operator.log.debug("updating")

        if operator.config["benchmark"]:
            ts = time.process_time_ns()
            dt = (ts - operator.view.time_last) / 1000000
            operator.view.time_last = ts
            print("events.updategtktree", dt, "ms")

        fp = operator.model.tabs[self.ident]["file"]
        if fp.endswith(".rst"):
            dtree = labnote.transform.rst2dtree(self.text, operator.config)
        if fp.endswith(".md"):
            dtree = labnote.transform.md2dtree(self.text, operator.config)

        if operator.config["benchmark"]:
            ts = time.process_time_ns()
            dt = (ts - operator.view.time_last) / 1000000
            operator.view.time_last = ts
            print("rst2dtree", dt, "ms")

        operator.model.update_gtkview(self.ident, dtree, self.line)


class Open(Event):

    def __init__(self, ident, fp, line=0):
        self.ident = ident
        # relative to base path
        self.fp = fp
        self.line = line

    def process(self, operator):
        operator.log.debug("opening {}#{}".format(self.fp, self.line))
        operator.model.open(self.ident, self.fp, self.line)


class Unmodify(Event):

    def __init__(self, ident):
        self.ident = ident

    def process(self, operator):
        operator.model.unmodify(self.ident)


class GoBack(Event):

    def __init__(self, ident):
        self.ident = ident

    def process(self, operator):
        operator.model.git.commit()
        operator.model.back(self.ident)


class GoHome(Event):

    def __init__(self, ident):
        self.ident = ident

    def process(self, operator):
        tab = operator.model.get_tab(self.ident)
        if tab:
            operator.model.git.commit()
            fp = tab["home"]
            # needs buffer modified check
            operator.model.load(self.ident, fp)
        else:
            # open new tab
            fp = operator.model.get_base_file()
            operator.model.open_tab(fp)


class OpenExternal(Event):

    def __init__(self, uri):
        self.uri = uri

    def process(self, operator):
        # xdg-mime query filetype /home/thomas/Projects/labnote2/index.rst
        # xdg-mime query default text/x-rst
        # /usr/share/mime/packages/freedesktop.org.xml
        operator.log.debug("opening externally " + self.uri)

        if sys.platform != "win32":
            ret = subprocess.call(
                ["/usr/bin/xdg-open", self.uri],
                stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
            )
        else:
            ret = subprocess.call("start {}".format(self.uri), shell=True)

        operator.log.debug("returned {}".format(ret))


class OpenCurDir(Event):

    def __init__(self, ident):
        self.ident = ident

    def process(self, operator):
        operator.log.debug("opening the cur tab dir in file manager")
        tab = operator.model.get_tab(self.ident)
        if not tab:
            return
        fp = tab["file"]
        fp = os.path.dirname(fp)
        cwd = os.getcwd()
        uri = "file://" + os.path.join(cwd, fp)
        operator.log.debug(uri)
        operator.enqueue(OpenExternal(uri))


class PrevTab(Event):

    def __init__(self, ident):
        self.ident = ident

    def process(self, operator):
        operator.log.debug("switch to previous tab")
        operator.view.prev_tab(self.ident)


class NextTab(Event):

    def __init__(self, ident):
        self.ident = ident

    def process(self, operator):
        operator.log.debug("switch to next tab")
        operator.view.next_tab(self.ident)


# TODO hash (on disk) file before overwriting and compare with hash in model to prevent overwriting updated files
class Save(Event):

    def __init__(self, ident, fp=None):
        self.ident = ident
        self.fp = fp

    def process(self, operator):
        tab = operator.model.get_tab(self.ident)
        if not tab:
            return

        if not self.fp:
            fp = tab["file"]
        else:
            # TODO window title is not updated
            # create model function to properly change cwd?
            dp, fp = os.path.split(self.fp)
            os.chdir(dp)
            tab["file"] = fp
            # TODO copy all files from tmp dir to target

        if os.getcwd().startswith("/tmp/labnote2") and fp.endswith("index.rst"):
            operator.log.debug("start file chooser")
            operator.model.view.dialog_save_as(self.ident)
            return

        operator.log.debug("saving %s", fp)
        text = operator.model.get_text(self.ident)

        if sys.platform != "win32":
            ok = self.save_linux(operator, fp, text)
        else:
            ok = self.save_windows(operator, fp, text)

        if not ok:
            # TODO check if no space left -> report
            operator.view.status_set("failed to save file")
            return

        # for generated files
        #self.save_cache(operator, fp)

        operator.model.unmodify(self.ident)

        # add saved file to git
        operator.model.git.add(fp)

        operator.log.debug("saved file")
        operator.view.status_set_timeout("saved", 1)

    def save_cache(self, operator, fp):
        operator.log.debug("saving generated files")
        curdir = os.path.dirname(fp)
        # TODO
        # save only "registered" files?
        cachedir = "/tmp/labnote.cache"
        cachedir = os.path.join(cachedir, curdir)
        if not os.path.exists(cachedir):
            return
        for f in os.listdir(cachedir):
            s = os.path.join(cachedir, f)
            d = os.path.join(curdir, f)
            # FIXME error in path handling (wrong cur/basedir?)
            shutil.move(s, d)  # FIXME crashes if destination path already exists

        # TODO remove old versions (according to naming scheme?)

    def save_linux(self, operator, fp, text):
        # write-replace (unix style)
        fn = os.path.basename(fp)
        dn = os.path.dirname(fp)
        tfn = "." + fn + ".swp"
        tfp = os.path.join(dn, tfn)

        # create subdir (on save) if nonexistent
        if dn:
            if not os.path.exists(dn):
                os.makedirs(dn, exist_ok=True)

        try:
            tfd = os.open(tfp, os.O_WRONLY | os.O_CREAT | os.O_EXCL, 0o600)
        except FileExistsError:
            operator.log.error("failed to save file, tmp file already exists")
            return False

        # TODO explicitly abort if write fails
        with os.fdopen(tfd, "w") as tf:
            tf.write(text)
            #tf.flush()

        # TODO would fsync be necessary here?
        #os.fsync(tfd)

        try:
            os.rename(tfp, fp)
        except OSError:
            operator.log.error("failed to save file, cannot move")
            return False

        return True

    def save_windows(self, operator, fp, text):
        # create subdir (on save) if nonexistent
        dn = os.path.dirname(fp)
        if dn:
            if not os.path.exists(dn):
                os.makedirs(dn, exist_ok=True)

        try:
            f = open(fp, "w", encoding="utf-8")
        except Exception:
            return False

        f.write(text)
        f.close()

        return True


class Export(Event):

    def __init__(self, ident, template):
        self.ident = ident
        self.template_fp = template

    def process(self, operator):
        tab = operator.model.get_tab(self.ident)
        if not tab:
            return

        rst_fp = tab["file"]

        meta = {}
        meta["title"] = rst_fp

        rev = operator.model.git.get_rev(rst_fp)
        if rev:
            modified = operator.view.buffer_is_modified(self.ident)
            dirty = operator.model.git.is_dirty(rst_fp)
            if modified or dirty:
                dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
                rev += " changed"
            else:
                dt = operator.model.git.get_dt(rst_fp)
        else:
            rev = ""
            dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")

        meta["date"] = dt
        meta["rev"] = rev

        operator.log.debug("export %s using %s", self.ident, self.template_fp)

        operator.view.status_set("exporting...")
        rst = operator.model.get_text(self.ident)

        pdf_fp = os.path.join(tempfile.gettempdir(), "labnote.pdf")

        curdir = os.path.dirname(tab["file"])

        ok = labnote.transform.rst2pdf(operator, rst, rst_fp, pdf_fp, self.template_fp, meta, curdir)
        if ok:
            operator.enqueue(labnote.events.OpenExternal(pdf_fp))
            operator.view.status_set_timeout("export done", 3)
        else:
            operator.view.status_set_timeout("export failed", 5)


class FocusEditor(Event):

    def process(self, operator):
        operator.view.focus_editor()


class FocusViewer(Event):

    def process(self, operator):
        operator.view.focus_viewer()


"""
class ScrollEditor(Event):

    def __init__(self, ident, line):
        self.ident = ident
        self.line = line

    def process(self, operator):
        operator.view.scroll_editor(self.ident, self.line)

"""
class ScrollViewer(Event):

    def __init__(self, ident, line=-1):
        self.ident = ident
        self.line = line

    def process(self, operator):
        operator.view.scroll_viewer(self.ident, self.line)


class Search(Event):

    def __init__(self, ident, term):
        self.ident = ident
        self.term = term

    def process(self, operator):
        operator.model.search_start(self.ident, self.term)


class SearchStop(Event):

    def __init__(self, ident):
        self.ident = ident

    def process(self, operator):
        operator.log.debug("stopping search %s", self.ident)
        operator.model.search_stop(self.ident)


class GITinit(Event):

    def process(self, operator):
        operator.model.git_init()


class GITpull(Event):

    def process(self, operator):
        operator.view.status_set("waiting 30s for git pull")
        operator.model.git.pull(timeout=30)
        operator.view.status_clear()


class LoadPlugins(Event):

    def __init__(self, dps):
        self.dps = dps

    def process(self, operator):
        operator.log.debug("loading plugins from %s", self.dps)
        operator.model.load_plugins(self.dps)


class LoadTemplates(Event):

    def __init__(self, dps):
        self.dps = dps

    def process(self, operator):
        operator.log.debug("loading templates from %s", self.dps)
        operator.model.load_export_templates(self.dps)
        operator.model.load_rst_templates(self.dps)


class RegisterScheme(Event):

    def __init__(self, scheme, cb):
        self.scheme = scheme
        self.cb = cb

    def process(self, operator):
        operator.log.debug("registering {}".format(self.scheme))
        operator.view.register_scheme(self.scheme, self.cb)


