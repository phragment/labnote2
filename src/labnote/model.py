# This file is part of LabNote2
# Copyright 2019-2021 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import importlib
import importlib.machinery
import os
import re
import sys
import threading
# debug
import time

import labnote.events
import labnote.git
import labnote.refs
import labnote.gtkdtree


class Model:

    def __init__(self, log, config, view):

        self.log = log.getChild(type(self).__name__)
        self.config = config
        self.view = view

        self.basepath = ""
        self.tabs = {}
        self.ids = 1

        if self.config["renderer"] == "gtk":
            self.cache_math = labnote.gtkdtree.MathCache()
            self.cache_img = labnote.gtkdtree.ImageCache()

        self.git = labnote.git.GIT(self)
        #self.git = labnote.git.GIT2(self)

        self.threads = {}
        self.thread_run = {}

    def set_base(self, basepath):
        self.basepath = basepath
        self.view.set_win_title(labnote.refs.shorten_user(basepath))

    def get_base(self):
        return self.basepath

    def set_base_file(self, filepath):
        self.basefile = filepath

    def get_base_file(self):
        return self.basefile

    def get_tab(self, ident):
        try:
            tab = self.tabs[ident]
        except KeyError:
            tab = None
        return tab

    def load(self, ident, fp, line=0):
        # internal, used by open and open_tab
        self.log.debug("loading {}".format(fp))

        self.view.status_clear()
        try:
            # explicit utf8 (windows), replace "illegal bytes" and force universal newline mode
            with open(fp, "r", encoding="utf-8", errors="replace", newline=None) as f:
                text = f.read()
        except FileNotFoundError:
            self.log.warning("file not found: " + fp)
            self.view.status_set("newly created")
            text = ""

        # TODO hash file contents and save for later comparison
        self.tabs[ident]["file"] = fp

        self.view.update_tab(ident, fp, text, line)

    def open_tab(self, fp, line=0):
        self.log.debug("opening new tab " + fp)

        # only open a file once
        for ident in self.tabs:
            if self.tabs[ident]["file"] == fp:
                # TODO switch to tab
                return

        ident = self.ids
        self.ids += 1

        self.tabs[ident] = {}
        self.tabs[ident]["file"] = fp
        self.tabs[ident]["history"] = []
        self.tabs[ident]["home"] = fp

        # wont race, both calls async via @gtk
        self.view.create_tab(ident, fp)
        self.load(ident, fp, line)

    def close_tab(self, ident):
        self.log.debug("closing " + str(ident))

        # check if buffer modified
        if self.view.buffer_is_modified(ident):
            self.view.info_ask("Discard changes?",
                               [labnote.events.Unmodify(ident),
                                labnote.events.CloseTab(ident)])
            return

        del self.tabs[ident]
        self.view.close_tab(ident)

    def open(self, ident, fp, line):
        self.log.debug("opening " + fp)

        tab = self.get_tab(ident)
        if not tab:
            self.open_tab(fp)
            return

        op = tab["file"]
        # dont reopen self
        #if op == fp:
        #    return

        # TODO send line
        # check if buffer modified
        if self.view.buffer_is_modified(ident):
            self.view.info_ask("Discard changes?",
                               [labnote.events.Unmodify(ident),
                                labnote.events.Open(ident, fp)])
            return

        self.load(ident, fp, line)

        # dont save same file in history
        if op == tab["file"]:
            return
        self.tabs[ident]["history"].append(op)

    def back(self, ident):
        self.log.debug("go back " + str(ident))

        tab = self.get_tab(ident)
        if not tab:
            return

        if not tab["history"]:
            return

        fp = tab["history"][-1]

        # check if buffer modified
        if self.view.buffer_is_modified(ident):
            self.view.info_ask("Discard changes?",
                               [labnote.events.Unmodify(ident),
                                labnote.events.GoBack(ident)])
            return

        del self.tabs[ident]["history"][-1]

        self.load(ident, fp)

    def get_text(self, ident):
        """
        use with caution (sync access to view thread)
        """
        return self.view.get_text(ident)

    def unmodify(self, ident):
        self.view.buffer_unmodify(ident)

    # TODO rename to update_webview?
    def load_html(self, ident, html):
        if self.view.config["benchmark"]:
            ts = time.process_time_ns()
            dt = (ts - self.view.time_last) / 1000000
            self.view.time_last = ts
            print("model.load_html", dt, "ms")

        self.view.load_html(ident, html)

    def load_export_templates(self, dirs):
        fps = self._list_dirs(dirs, ".tex")

        fn = self.config["template-default"]

        self.log.debug("export templates: %s", fps)
        self.view.create_export_menu(fps, fn)

    def load_rst_templates(self, dirs):
        fps = self._list_dirs(dirs, ".rst")
        self.log.debug("found rst templates: %s", fps)
        self.view.create_templates_menu(fps)

    def load_plugins(self, dirs):
        directive_fps = self._list_dirs(dirs, ".py")

        self.log.debug("loading plugins: %s", directive_fps)

        for fp in directive_fps:
            loader = importlib.machinery.SourceFileLoader("test", fp)
            try:
                handle = loader.load_module("test")
            except Exception:
                continue
            handle.register()

    def _list_dirs(self, dirs, ext):
        fps = []
        fns = []

        for dp in dirs:
            try:
                files = os.listdir(dp)
            except Exception:
                continue

            for fn in files:
                if not fn.endswith(ext):
                    continue

                # ignore if filename already in list
                if fn in fns:
                    continue

                fp = os.path.join(dp, fn)
                if not os.path.exists(fp):
                    continue

                fps.append(fp)
                fns.append(fn)

        return fps

    def find_regex(self, lines, regex, ignorecase=True):
        pattern = re.compile(regex)
        if ignorecase:
            pattern = re.compile(regex, flags=re.IGNORECASE)

        results = []
        for lineno, line in enumerate(lines):
            if pattern.search(line):
                r = [lineno + 1, line.strip()]
                results.append(r)

        return results

    def find_substring(self, lines, searchstr, ignorecase=True):
        searchstr = searchstr.strip()
        if ignorecase:
            searchstr = searchstr.lower()

        results = []
        for lineno, line in enumerate(lines):
            if ignorecase:
                line = line.lower()

            if searchstr in line:
                r = [lineno + 1, line.strip()]
                results.append(r)

        return results

    def find_simple(self, lines, searchstr):
        searchstrings = searchstr.strip().lower().split()

        results = []
        for lineno, line in enumerate(lines):
            line_ = line.lower()

            # or
            #if any(s in line_ for s in searchstrings):
            # and
            if all(s in line_ for s in searchstrings):
                r = [lineno + 1, line.strip()]
                results.append(r)

        return results

    def search(self, ident, pattern):
        """go recursively through all .rst files
        down from the current subdir using substring
        """

        self.view.search_clear(ident)

        if not pattern:
            return

        dp = "."
        subdir = os.path.dirname(self.tabs[ident]["file"])
        dp = os.path.join(dp, subdir)

        self.log.debug("searching for %s in %s", pattern, dp)

        i = 0
        for parent, dirs, files in os.walk(dp):
            for fn in files:

                if not self.thread_run[ident]:
                    self.log.debug("searching for %s stopped", ident)
                    return

                filepath = os.path.join(parent, fn)
                if filepath.endswith(".rst"):
                    with open(filepath) as f:
                        fp = filepath[len(dp):]
                        if fp.startswith("/"):
                            fp = fp[1:]  # hacky af
                        short = labnote.refs.shorten_ref(fp, len_max=30)

                        #find_res = self.find_regex(f, pattern)
                        #find_res = self.find_substring(f, pattern)
                        find_res = self.find_simple(f, pattern)
                        for found in find_res:
                            res = [short]
                            res.extend(found)
                            res.append(filepath)
                            self.view.search_add(ident, res)

                        i += 1

        self.log.debug("searching for %s done", ident)
        self.view.search_add(ident, ["done", i, "files searched", ""])

    def search_start(self, ident, pattern):
        # stop currently running search
        self.search_stop(ident)

        thread = threading.Thread(
            target=self.search,
            name="SearchThread",
            args=(ident, pattern)
        )
        self.threads[ident] = thread
        self.thread_run[ident] = True
        thread.start()

    def search_stop(self, ident):
        thread = self.threads.get(ident, None)
        if thread:
            self.thread_run[ident] = False
            thread.join()
            self.view.search_add(ident, ["stopped", 0, "", ""])

    def git_init(self):
        self.git.init(self.get_base())

    def update_gtkview(self, ident, dtree, line):
        curdir = os.path.dirname( self.tabs[ident]["file"] )
        entries = labnote.gtkdtree.convert(dtree, self.cache_img, self.cache_math, curdir)
        self.view.update_gtkview(ident, entries, line)



