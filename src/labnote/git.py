# This file is part of LabNote2
# Copyright 2019-2021 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import os

import labnote.utils


class GIT:

    def __init__(self, model):
        self.model = model
        self.log = self.model.log.getChild(type(self).__name__)
        self.config = self.model.config

        self.repo = False

    def init(self, dp):
        self.log.debug("initialize git")

        gp = os.path.join(dp, ".git")
        if os.path.isdir(gp):
            self.repo = True

    def add(self, fp):
        if not self.repo:
            return

        self.log.debug("adding {}".format(fp))
        ret, out = labnote.utils.run(["git", "add", fp])

        if ret != 0:
            self.log.error(out)

    def commit(self):
        if not self.repo:
            return

        self.log.debug("committing")
        ret, out = labnote.utils.run(["git", "commit", "--allow-empty-message", "-m", ""])

        if ret != 0:
            self.log.error(out)

    def push(self, timeout=0):
        if not self.repo:
            return

        ret, out = labnote.utils.run(["git", "ls-remote"])
        if ret != 0:
            #self.log.warning(out)
            # 128 no remotes
            self.log.debug("not going to push (no remotes)")
            return

        self.log.debug("pushing")

        # 60s is too long for typical usage and too short for large pushes
        ret, out = labnote.utils.run(["git", "push"], timeout=timeout)
        if ret != 0:
            self.log.error(out)
        else:
            self.log.debug(out)

    def pull(self, timeout=0):
        if not self.repo:
            return

        ret, out = labnote.utils.run(["git", "ls-remote"])
        if ret != 0:
            self.log.debug("not going to pull (no remotes)")
            return

        self.log.debug("pulling")

        ret, out = labnote.utils.run(["git", "pull"], timeout=timeout)
        if ret != 0:
            self.log.error(out)
        else:
            self.log.debug(out)

    def is_dirty(self, fp):
        ret, state = labnote.utils.run(["git", "status", "--porcelain", fp])
        # M   modified
        # ??  untracked
        if ret == 0:
            if state.startswith("M"):
                return True
        return False

    def get_dt(self, fp):
        err, date = labnote.utils.run(["git", "log", "-1", "--date=short-local", "--format=%cd", fp])
        if err:
            return None
        return date

    def get_rev(self, fp):
        err, cnt = labnote.utils.run(["git", "rev-list", "--count", "HEAD"])
        if err:
            return None
        err, hsh = labnote.utils.run(["git", "rev-parse", "--short", "HEAD"])
        if err:
            return None
        rev = "r" + cnt + "." + hsh
        return rev


class GIT2:
    #import pygit2

    def __init__(self, model):
        import pygit2

        self.model = model
        self.log = self.model.log.getChild(type(self).__name__)
        self.config = self.model.config

    def init(self, dp):
        self.log.debug("initialize git using PyGIT (libgit)")
        try:
            self.repo = self.pygit2.Repository(dp)
        except self.pygit2.GitError:
            self.log.debug("{} not a git dir".format(dp))
            self.repo = None
            return

        # test
        try:
            head = self.repo.head.target
        except self.pygit2.GitError:
            return

        commit = self.repo[self.repo.head.target]
        shash = commit.hex[:7]
        msg = commit.message.strip()
        self.log.debug("commit {} {}".format(shash, msg))

    def add(self, fp):
        if not self.repo:
            return
        self.log.debug("adding {}".format(fp))

    def commit(self):
        if not self.repo:
            return
        self.log.debug("committing")

    def push(self):
        if not self.repo:
            return
        self.log.debug("pushing")

    def get_info(self, fp):
        if not self.repo:
            return
        self.log.debug("query {}".format(fp))




