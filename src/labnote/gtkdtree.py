# This file is part of LabNote2
# Copyright 2022-2024 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import os
import shutil
import tempfile
import urllib
import io
import re
import sys
import time

import docutils
import docutils.core
import docutils.nodes

import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, Gdk, GLib, GdkPixbuf, Pango, GObject

import matplotlib
import matplotlib.pyplot
import matplotlib.mathtext

# labnote
import labnote.utils
import labnote.refs

"""
TODO
 - implement more nodes
   - tables
 - indent elements according to level (block_quote)
 - section ids
 - add support for video
"""

"""
currently not planned to implement
- inline images
- deeply nested elements
  - eg in tables (bullet lists, etc)

"""

def convert(dtree, cache_img, cache_math, curdir):
    args = {
        "_disable_config": True
    }

    output, publisher = docutils.core.publish_programmatically(
        source_class=docutils.io.DocTreeInput,
        source=dtree,
        source_path=None,
        destination_class=docutils.io.StringOutput,
        destination=None,
        destination_path=None,
        reader=docutils.readers.doctree.Reader(), reader_name="",
        parser=None, parser_name="restructuredtext",
        writer=WidgetWriter(cache_img, cache_math, curdir), writer_name="",
        settings=None, settings_spec=None,
        settings_overrides=args,
        config_section=None,
        enable_exit_status=None
    )

    #for entry in output:
    #    print(entry)

    return output


class WidgetWriter(docutils.writers.Writer):

    def __init__(self, cache_img, cache_math, curdir):
        super().__init__()
        self.cache_img = cache_img
        self.cache_math = cache_math
        self.curdir = curdir

    def translate(self):
        visitor = WidgetVisitor(self.document, self.cache_img, self.cache_math, self.curdir)
        self.document.walkabout(visitor)
        self.output = visitor.output


class WidgetVisitor(docutils.nodes.NodeVisitor):

    def __init__(self, document, cache_img, cache_math, curdir):
        super().__init__(document)

        self.cache_img = cache_img
        self.cache_math = cache_math
        self.curdir = curdir

        # list of entries
        self.output = []

        # states
        self.buffer = []

        self.refuri = None
        self.refid = None

        self.lvl_section = 0
        self.lvl_blist = 0
        self.lvl_elist = 0

        self.lvl_indent = 0

        self.elist_start = 0
        self.elist_item = 0

        self.table = []
        self.table_row = 0
        self.table_col = 0

        self.field_name = ""

        # flags
        self.preserve_whitespace = False
        self.disable_image = False
        self.disable_paragraph = False

        self.last_image = None
        self.last_caption = None
        self.last_lineno = -1

        #
        self.remove_ws = re.compile(" +")

        self.blist_markers = ["•", "◦", "▪", "▫", "▸", "▹", "◆", "◇", "◈"]

        self.fields = {
            "title": "Title",
            "subtitle": "Subtitle",
            "author": "Author",
            "date": "Date",
            "topic": "Topic",
        }

    def buf_add(self, s):
        # strip leading and trailing whitespace, remove double spaces
        # only use with GLib.markup_escape_text
        self.buffer.append(s)

    def buf_get(self):
        # only use with Gtk.Label.set_markup
        buf = "".join(self.buffer)
        self.buffer = []
        return buf

    def buf_clr(self):
        self.buffer = []

    # -------------------------------------------------------------------------
    # docutils interface
    def __getattr__(self, name):
        #print("TODO implement:", name)
        return self.ignore

    def ignore(self, node):
        return

    # -------------------------------------------------------------------------
    # structural elements

    # document
    def visit_document(self, node):
        pass

    def depart_document(self, node):
        pass

    # section
    def visit_section(self, node):
        self.lvl_section += 1
        # TODO lineno

    def depart_section(self, node):
        self.lvl_section -= 1

    # transition
    def visit_transition(self, node):
        entry = Entry()
        #entry.text = "----"
        entry.lineno = node.line
        entry.name = "transition"
        self.output.append(entry)

    # -------------------------------------------------------------------------
    # structural subelements

    # title
    #   can be child of section, sidebar, table, more?
    #   TODO check if parent is document?
    def visit_title(self, node):
        pass

    def depart_title(self, node):
        text = self.buf_get()
        entry = Entry()
        entry.text = text
        entry.lineno = node.line
        if node.parent.tagname == "section":
            entry.name = "h{}".format(self.lvl_section)
        self.output.append(entry)

    # -------------------------------------------------------------------------
    # body elements, simple

    # comment
    def depart_comment(self, node):
        self.buf_clr()

    # raw
    def depart_raw(self, node):
        #form = node.get("format", "")
        self.buf_clr()

    # paragraph
    def visit_paragraph(self, node):
        #self.disable_image = True  # allow inline images for markdown
        pass

    def depart_paragraph(self, node):
        self.disable_image = False

        if self.disable_paragraph:
            return

        text = self.buf_get()
        entry = Entry()
        entry.text = text
        entry.lineno = node.line
        entry.css_class = "indent{}".format(self.lvl_indent)

        self.output.append(entry)
        #if self.disable_paragraph:
        #    self.last_paragraph = entry
        #else:
        #    self.output.append(entry)

    # text (only leaf node in doctree)
    def visit_Text(self, node):
        text = node.astext()

        # prepare
        if not self.preserve_whitespace:
            # https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html#whitespace

            # replace newline with space
            text = text.replace("\n", " ")

            # collapse multiple spaces
            text = self.remove_ws.sub(" ", text)

        text = GLib.markup_escape_text(text)

        # FIXME needed?
        # put whitespace of link text behind link
        if self.refuri or self.refid:
            t = text.strip()
            pad = ""
            if len(t) != len(text):
                pad = " " * (len(text) - len(t))
                #pad = " "
                text = t

        if self.refuri:
            # prepare uri
            uri = labnote.refs.convert_ref2laburi(self.refuri, self.curdir)
            if not uri:
                uri = self.refuri

            self.buf_add("<a href=\"{}\">{}</a>{}".format(uri, text, pad))
            self.refuri = None
            return

        if self.refid:
            # TODO create uri
            self.buf_add("<a href=\"#{}\">{}</a>{}".format(self.refid, text, pad))
            self.refid = None
            return

        self.buf_add(text)

    # literal_block
    def visit_literal_block(self, node):
        self.preserve_whitespace = True

    def depart_literal_block(self, node):
        self.preserve_whitespace = False
        text = self.buf_get()

        # ???
        #text = GLib.markup_escape_text(text)

        entry = Entry()
        entry.name = "literal"
        entry.lineno = node.line
        entry.text = text
        self.output.append(entry)


    # image
    def visit_image(self, node):
        #if self.disable_image:
        #    return

        # nope
        #print("image", node.line)

        height = node.get("height", None)
        width = node.get("width", None)
        target = node.get("target", "")
        alt = node.get("alt", "")
        uri = node.get("uri", "")

        # TODO
        fp = labnote.refs.convert_ref2fp(uri, self.curdir)
        fp_ref = labnote.refs.convert_ref2laburi(self.refuri, self.curdir)

        entry = Entry()
        entry.fp_ref = fp_ref
        pb = self.cache_img.get_pb(fp)
        entry.pixbuf = pb

        if self.disable_image:
            self.last_image = entry
        else:
            self.output.append(entry)

    # caption
    def depart_caption(self, node):
        #print("caption", node.line)
        self.last_lineno = node.line
        self.last_caption = self.buf_get()

    # math_block
    def depart_math_block(self, node):
        entry = Entry()
        entry.lineno = node.line
        tex = self.buf_get()
        pb = self.cache_math.get_pb(tex)
        entry.pixbuf = pb
        self.output.append(entry)

    # pending
    def visit_pending(self, node):
        # TODO check if node is intended for this writer
        pass

    def depart_pending(self, node):
        pass

    # block_quote
    def visit_block_quote(self, node):
        self.lvl_indent += 1

    def depart_block_quote(self, node):
        self.lvl_indent -= 1

    # list_item
    def visit_list_item(self, node):
        self.lvl_indent += 1

        if self.lvl_blist > 0:
            i = self.lvl_blist - 1
            text = '<big>'
            try:
                text += self.blist_markers[i]
            except IndexError:
                text += "-"
            text += '</big>'
            text += "  "
            self.buf_add(text)

        if self.lvl_elist > 0:
            i = self.lvl_elist - 1
            text = str(self.elist_start + self.elist_item)
            text += ". "
            self.buf_add(text)
            self.elist_item += 1

    def depart_list_item(self, node):
        self.lvl_indent -= 1
        # remove marker if there is no content
        self.buf_clr()

    # -------------------------------------------------------------------------
    # body elements, compound

    # figure
    def visit_figure(self, node):
        self.disable_image = True
        self.disable_paragraph = True

    def depart_figure(self, node):
        self.disable_image = False
        self.disable_paragraph = False

        entry = self.last_image
        if self.last_caption:
            entry.text = self.last_caption
        entry.lineno = self.last_lineno

        # no lineno for docutils figure!
        #print("figure", node.line)

        self.output.append(entry)

        #self.last_paragraph = None
        self.last_image = None
        self.last_caption = None


    # bullet_list
    def visit_bullet_list(self, node):
        self.lvl_blist += 1

    def depart_bullet_list(self, node):
        self.lvl_blist -= 1

    # enumerated_list
    def visit_enumerated_list(self, node):
        self.lvl_elist += 1
        self.elist_start = int( node.get("start", "1") )
        self.elist_item = 0
        # <enumerated_list enumtype="arabic" prefix="" suffix=".">
        # <enumerated_list enumtype="loweralpha" prefix="" suffix=")">

    def depart_enumerated_list(self, node):
        self.lvl_elist -= 1

    # inline elements

    # inline
    def visit_inline(self, node):
        # used for syntax highlighting
        #print(node["classes"])

        text = ""
        for cls in node["classes"]:
            # comment
            if cls == "comment":
                text += ' foreground="#888a85" font_style="italic"'

            # statement
            if cls == "keyword":
                text += ' foreground="#ffffff" font_weight="bold"'

            # identifier
            if cls == "name":
                text += ' foreground="#729fcf"'

            # string
            if cls == "string":
                text += ' foreground="#edd400"'

        if not text:
            return

        text = f'<span{text}>'
        self.buf_add(text)

    def depart_inline(self, node):
        impl = ["comment", "keyword", "name", "string"]
        if not set(node["classes"]) & set(impl):
            return

        self.buf_add('</span>')

    # table
    def visit_table(self, node):
        self.disable_paragraph = True
        self.table = []
        self.table_row = 0

    def depart_table(self, node):
        self.disable_paragraph = False

        #print(self.table)
        entry = Entry()
        entry.name = "table"
        entry.table = self.table
        self.output.append(entry)

    ## body subelements, compound
    # table header
    #def visit_thead(self, node):

    #def depart_thead(self, node):

    # table row
    def visit_row(self, node):
        self.table.append([])

    def depart_row(self, node):
        self.table_row += 1

    # table? entry
    #def visit_entry(self, node):

    def depart_entry(self, node):
        text = self.buf_get()
        self.table[self.table_row].append(text)

    # -------------------------------------------------------------------------
    # body elements, inline

    # reference
    def visit_reference(self, node):
        # name refuri
        # ids refid
        self.refuri = node.get("refuri", None)
        self.refid = node.get("refid", None)

    def depart_reference(self, node):
        self.refuri = None
        self.refid = None

    # strong
    def visit_strong(self, node):
        self.buf_add("<span font_weight=\"bold\">")

    def depart_strong(self, node):
        self.buf_add("</span>")

    # emphasis
    def visit_emphasis(self, node):
        self.buf_add("<span font_style=\"italic\">")

    def depart_emphasis(self, node):
        self.buf_add("</span>")

    # literal
    def visit_literal(self, node):
        self.preserve_whitespace = True
        self.buf_add("<span font_family=\"monospace\">")

    def depart_literal(self, node):
        self.preserve_whitespace = False
        self.buf_add("</span>")

    # subscript
    def visit_subscript(self, node):
        self.buf_add("<sub>")

    def depart_subscript(self, node):
        self.buf_add("</sub>")

    # superscript
    def visit_superscript(self, node):
        self.buf_add("<sup>")

    def depart_superscript(self, node):
        self.buf_add("</sup>")

    # -------------------------------------------------------------------------
    # bibliographic elements

    # author
    def visit_author(self, node):
        self.field_name = "author"

    def depart_author(self, node):
        self.depart_field_body(node)

    # date
    def visit_date(self, node):
        self.field_name = "date"

    def depart_date(self, node):
        self.depart_field_body(node)

    # field
    def visit_field(self, node):
        self.disable_paragraph = True

    def depart_field(self, node):
        self.disable_paragraph = False

    # field_name
    #def visit_field_name(self, node):

    def depart_field_name(self, node):
        self.field_name = self.buf_get()

    # field_body
    def depart_field_body(self, node):
        text = self.fields.get(self.field_name, None)
        if not text:
            #print("TODO unknown field name:", self.field_name)
            text = self.field_name
        text += ":\t"
        text += self.buf_get()
        entry = Entry()
        entry.name = "biblio"
        entry.text = text
        entry.lineno = node.line
        self.output.append(entry)
        self.field_name = ""


# listmodel entry
class Entry(GObject.Object):

    def __init__(self, name="", text="", pixbuf=None, lineno=-1, fp="", fp_ref=""):
        super().__init__()
        self.name = name
        self.text = text
        self.pixbuf = pixbuf
        self.table = []
        self.css_class = ""

        # directly update in listmodel? (do not remove and readd element)
        self.lineno = lineno
        self.fp = fp
        self.fp_ref = fp_ref

        self.hash_cached = None

    # used for updating gtkview
    # only compare values which are displayed
    def __eq__(self, entry):
        if self.name != entry.name:
            return False
        if self.text != entry.text:
            return False
        if self.pixbuf != entry.pixbuf:
            return False
        if self.table != entry.table:
            return False
        if self.css_class != entry.css_class:
            return False
        return True

    def __hash__(self):
        if self.hash_cached:
            return self.hash_cached

        self.hash_cached = hash( (self.name, self.text, self.pixbuf, tuple(self.table)) )
        return self.hash_cached

    def update(self, entry):
        self.lineno = entry.lineno
        self.fp = entry.fp
        self.fp_ref = entry.fp_ref


# listview widget
class EntryWidget:

    def __init__(self, cb_activate_link, cb_activate_link_tab):
        self.cb_activate_link = cb_activate_link

        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.box.set_name("elem")
        self.box.props.vexpand = True

        #self.img = Gtk.Image()
        self.img = Gtk.Picture()
        self.img.props.vexpand = True
        self.img.props.hexpand = True
        self.img.set_cursor_from_name("pointer")
        #self.img.set_request_mode(Gtk.SizeRequestMode.CONSTANT_SIZE)
        self.box.append(self.img)

        self.sw = Gtk.ScrolledWindow()
        self.sw.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.NEVER)
        #self.sw.set_propagate_natural_height(True)
        self.sw.set_overlay_scrolling(False)
        self.box.append(self.sw)

        # workaround
        # dont capture vertical scroll events
        ctrls = self.sw.observe_controllers()
        for i in range(ctrls.get_n_items()):
            ctrl = ctrls.get_item(i)
            if isinstance(ctrl, Gtk.EventControllerScroll):
                ctrl.set_flags(Gtk.EventControllerScrollFlags.HORIZONTAL)
        del ctrls

        self.label = Gtk.Label()
        self.label.set_use_markup(True)
        #self.label.set_width_chars()  # set requested width in chars
        self.label.connect("activate-link", cb_activate_link)

        self.label.props.hexpand = True
        self.label.props.vexpand = True

        #self.box.append(self.label)
        self.sw.set_child(self.label)

        ## TODO create popover on bind?
        self.popover = Gtk.PopoverMenu()
        self.popover.set_autohide(True)
        self.popover.set_has_arrow(False)
        self.popover.set_parent(self.label)

        self.pobox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.popover.set_child(self.pobox)

        self.polabel = Gtk.Label.new("Open in Tab")
        self.polabel.set_xalign(0)
        self.polabel.props.hexpand = True
        self.polabel.props.margin_start = 5
        self.polabelbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        self.polabelbox.append(self.polabel)

        self.button = Gtk.Button.new()
        self.button.set_child(self.polabelbox)
        self.button.add_css_class("flat")
        self.pobox.append(self.button)

        self.button.connect("clicked", cb_activate_link_tab)

        self.grid = None

        self.box._entrywidget = self
        self.label._entrywidget = self
        self.button._entrywidget = self
        self.img._entrywidget = self

        self.entry = None
        self._uri = None

    def get_widget(self):
        return self.box

    def update(self, entry):
        self.box._entry = entry
        self._uri = None

        self.box.set_name(entry.name)  # TODO rename to css_id ?
        self.box.set_css_classes([entry.css_class])

        pb = entry.pixbuf
        text = entry.text

        if pb:
            texture = Gdk.Texture.new_for_pixbuf(pb)
            self.img.set_paintable(texture)
            self.img.props.height_request = pb.props.height
            self.img.set_visible(True)

            if text:
                self.label.props.halign = Gtk.Align.FILL
                self.label.props.wrap = Gtk.WrapMode.WORD_CHAR
                self.label.set_label(text)
                self.label.set_visible(True)
            else:
                self.label.set_visible(False)
        else:
            #self.img.clear()
            self.img.set_paintable(None)
            self.img.props.height_request = -1  # important to reset!
            self.img.set_visible(False)

            self.label.props.halign = Gtk.Align.START
            self.label.props.wrap = Gtk.WrapMode.WORD_CHAR
            self.label.set_label(text)
            self.label.set_visible(True)

        if entry.name == "literal":
            self.sw.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.NEVER)
            self.label.props.wrap = Gtk.WrapMode.NONE
            #self.sw.props.min-content-height
        else:
            self.sw.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.NEVER)

        #
        if self.grid:
            self.box.remove(self.grid)

        if entry.table:
            self.grid = Gtk.Grid()
            self.box.append(self.grid)

            for i, row in enumerate(entry.table):
                for j, cell in enumerate(row):
                    label = Gtk.Label()
                    label.props.use_markup = True
                    label.props.halign = Gtk.Align.START
                    label.props.hexpand = True
                    label.props.wrap = Gtk.WrapMode.WORD_CHAR
                    label.props.max_width_chars = 80
                    label.props.label = cell
                    label.connect("activate-link", self.cb_activate_link)
                    self.grid.attach(label, j, i, 1, 1)


class ImageCache:

    def __init__(self):
        self.cache = {}
        self.max_width = 600
        self.max_height = 600
        # 600x600 pixels 24bit color bmp is ~1MB
        self.size_limit = 100

    # TODO use funtools.lru_cache and add a size limit from config?
    def get_pb(self, fp):
        # try to load from cache
        pb = self.cache.get(fp, None)
        if pb:
            #print("cache hit")
            return pb

        #print("cache miss")

        # try to load from file
        pb = self.load_file(fp)
        if pb:
            if len(self.cache) > self.size_limit:
                k = next(iter(self.cache))
                self.cache.pop(k)

            self.cache[fp] = pb
            return pb

        # create image missing
        # TODO fix
        #display = Gdk.Display.get_default()
        #theme = Gtk.IconTheme.get_for_display(display)
        #tp = theme.get_search_path()
        # gtk4 "image-loading" ?
        #paintable = theme.lookup_icon("image-missing", None, 50, 1, Gtk.TextDirection.NONE, 0)
        # Gtk.IconLookupFlags.FORCE_REGULAR

        return pb

    def load_file(self, fp):
        #info = GdkPixbuf.Pixbuf.get_file_info(fp)

        try:
            pb = GdkPixbuf.Pixbuf.new_from_file(fp)
        except GLib.Error:
            pb = None

        # can pb be None without raising an exception?
        if not pb:
            return None

        pb = pb.apply_embedded_orientation()

        w = pb.get_width()
        h = pb.get_height()

        if w > self.max_width or h > self.max_height:
            # preserve aspect ratio
            # w / h == w_new / h_new
            if w > h:
                w_new = self.max_width
                h_new = h * w_new/w
            else:
                h_new = self.max_height
                w_new = w * h_new/h

            pb = pb.scale_simple(w_new, h_new, GdkPixbuf.InterpType.BILINEAR)

        return pb


class MathCache:

    def __init__(self):
        self.cache = {}
        self.dpi = 96  # 23" FHD
        #self.dpi = 120

    def get_pb(self, tex):
        pb = self.cache.get(tex, None)
        if not pb:
            pb = self.render(tex)
            if pb:
                self.cache[tex] = pb
        return pb

    # why ident?
    def get_pb_(self, tex, ident):
        tex_, pb = self.cache.get(tex, (None, None) )
        if not pb or tex != tex_:
            pb = self.render(tex)
            if pb:
                self.cache[ident] = (tex, pb)

        #print(tex, pb)
        return pb

    def render(self, eq):
        # TODO make configurable
        #return self.render_tex(eq)
        return self.render_mpl(eq)

    def render_tex(self, eq):
        template = r"""
        \documentclass[border=2pt,varwidth]{{standalone}}
        \usepackage{{standalone}}
        \usepackage{{amsmath}}
        \begin{{document}}
        \begin{{equation*}}
        {}
        \end{{equation*}}
        \end{{document}}
        """

        dp = tempfile.mkdtemp()
        tex = template.format(eq)

        with open(os.path.join(dp, "equation.tex"), "w") as f:
            f.write(tex)

        latex = ["latex", "-halt-on-error", "equation.tex"]
        r, err = labnote.utils.run(latex, cwd=dp, timeout=30)

        dvipng = ["dvipng", "equation.dvi", "-o", "equation.png",
            "-fg", "White",
            "-bg", "Transparent",
            "-D", str(self.dpi)]
        r, err = labnote.utils.run(dvipng, cwd=dp, timeout=30)

        pb = GdkPixbuf.Pixbuf.new_from_file(os.path.join(dp, "equation.png"))

        shutil.rmtree(dp)

        return pb

    def render_mpl(self, eq):
        matplotlib.pyplot.rcParams.update({"text.usetex": False})
        matplotlib.pyplot.rcParams.update({"mathtext.fontset": "dejavuserif"})
        # TODO make configurable
        matplotlib.pyplot.rcParams.update({"text.color": "#d3d7cf"})

        s = "$" + eq + "$"
        buf = io.BytesIO()

        #matplotlib.mathtext.math_to_image(s, buf, dpi=self.dpi, format="png")
        try:
            self.math_to_image(s, buf, dpi=self.dpi, format="png")
        except ValueError:
            return None

        loader = GdkPixbuf.PixbufLoader()
        loader.write(buf.getbuffer())
        loader.close()
        pb = loader.get_pixbuf()
        #print(pb.props.width, pb.props.height)

        return pb

    def math_to_image(self, s, filename_or_obj, prop=None, dpi=None, format=None):
        # stolen from matplotlib
        # we want a transparent background
        from matplotlib import figure
        from matplotlib.backends import backend_agg
        if prop is None:
            prop = matplotlib.mathtext.FontProperties()
        parser = matplotlib.mathtext.MathTextParser("path")
        width, height, depth, _, _ = parser.parse(s, dpi=dpi, prop=prop)
        #width *= 2
        #height *= 2
        #print(width, height, depth, dpi)
        fig = figure.Figure(figsize=(width / dpi, height / dpi))
        fig.text(0, depth/height, s, fontproperties=prop)
        backend_agg.FigureCanvasAgg(fig)
        fig.savefig(filename_or_obj, dpi=dpi, format=format,
            transparent=True)
        return depth


