# This file is part of LabNote2
# Copyright 2019-2021 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import os
import shutil
import tempfile
import logging

import docutils
import docutils.core

import labnote.refs
import labnote.utils

try:
    from myst_parser.docutils_ import Parser as MyST
except Exception:
    pass


def rst2dtree(rst, config):
    pargs = {
        "_disable_config": True,
        "doctitle_xform": False,
        "report_level": 5,
        "halt_level": 5
    }

    dtree = docutils.core.publish_doctree(rst, settings_overrides=pargs)

    if config["verbose"]:
        xml = docutils.core.publish_from_doctree(dtree, writer_name="pseudoxml")
        with open("/tmp/labnote2.dtree", "w") as f:
            f.write(xml.decode())

    return dtree


def md2dtree(md, config):
    pargs = {
        "_disable_config": True,
        "doctitle_xform": False,
        "report_level": 5,
        "halt_level": 5,
        "myst_enable_extensions": ["deflist"]
    }

    if not config["markdown"]:
        return rst2dtree("INFO install myst parser for markdown support", config)

    dtree = docutils.core.publish_doctree(
        source=md,
        settings_overrides=pargs,
        parser=MyST(),
    )

    if config["verbose"]:
        xml = docutils.core.publish_from_doctree(dtree, writer_name="pseudoxml")
        with open("/tmp/labnote2.dtree", "w") as f:
            f.write(xml.decode())

    return dtree


def dtree2html(dtree, config, cargs=None):
    args = {
        "_disable_config": True,
        "embed_stylesheet": True,
        "output_encoding": "unicode"
    }

    if cargs:
        args.update(cargs)

    stylesheet = config["style-webview"]
    if os.path.isfile(stylesheet):
        args["stylesheet_path"] = stylesheet

    html = docutils.core.publish_from_doctree(
        dtree, writer_name="html4css1", settings_overrides=args)

    return html


def rst2html(rst, curdir, config, pos, ident):
    # create document tree
    dtree = rst2dtree(rst, config)
    # prepare references
    dtree = dtree_prep(dtree, curdir, ident)
    # insert scroll mark
    dtree = dtree_insert(dtree, pos)
    # convert to html
    html = dtree2html(dtree, config)
    # insert scroll script
    html = html_prep(html)

    # debug
    if config["verbose"]:
        with open("/tmp/labnote.html", "w") as dbg:
            dbg.write(html)

    return html


def md2html(md, curdir, config, pos, ident):
    # create document tree
    dtree = md2dtree(md, config)
    # prepare references
    dtree = dtree_prep(dtree, curdir, ident)
    # insert scroll mark
    dtree = dtree_insert(dtree, pos)
    # convert to html
    html = dtree2html(dtree, config)
    # insert scroll script
    html = html_prep(html)

    # debug
    if config["verbose"]:
        with open("/tmp/labnote.html", "w") as dbg:
            dbg.write(html)

    return html


def dtree_prep(dtree, reldir, ident):
    for elem in dtree.traverse(siblings=True):
        if elem.tagname == "reference" or elem.tagname == "image":
            try:
                if elem.tagname == "reference":
                    refuri = elem["refuri"]
                if elem.tagname == "image":
                    refuri = elem["uri"]
            except KeyError:
                continue

            # FIXME workaround
            #if labnote.refs.is_video(refuri):
            #    print("video!")
            #    print(elem)
            #    continue

            scheme = "labnote-{}".format(ident)
            laburi = labnote.refs.convert_ref2laburi(refuri, reldir, scheme)
            if not laburi:
                continue

            if elem.tagname == "reference":
                elem["refuri"] = laburi
            if elem.tagname == "image":
                elem["uri"] = laburi

    return dtree


def dtree_prep_ref2fp(dtree, reldir):
    for elem in dtree.traverse(siblings=True):
        if elem.tagname == "reference":
            key = "refuri"
        elif elem.tagname == "image":
            key = "uri"
        else:
            continue

        ref = elem.get(key, None)
        if not ref:
            continue

        fp = labnote.refs.convert_ref2fp(ref, reldir)
        if not fp:
            continue
        print(ref, reldir, fp)

        elem[key] = fp

    return dtree


def dtree_insert(dtree, pos):
    html_mark = "<a id='scroll-mark'></a>"
    node_mark = docutils.nodes.raw(html_mark, html_mark, format="html")

    if pos == 0:
        return dtree

    elems = []
    for elem in dtree.traverse(siblings=True):
        if elem.line:
            elems.append(elem)

    blacklist = ["math_block"]

    # default: prepend mark
    # append for text?

    for elem in reversed(elems):
        if elem.line < pos:
            #print("append to", elem.tagname, elem.line)

            if elem.tagname in blacklist:
                continue

            elem += node_mark
            break

    # debug
    #pretty = docutils.core.publish_from_doctree(dtree, writer_name="pseudoxml")
    #with open("/tmp/labnote.dtree", "w") as f:
    #    f.write(pretty.decode())

    return dtree


def dtree_find_files(dtree, dp):
    files = []
    for elem in dtree.traverse(siblings=True):
        if elem.tagname == "image":
            ref = elem["uri"]
            # handled by dtree_prep_ref2fp
            #fp = labnote.refs.convert_ref2fp(ref, dp)
            #files.append(fp)
            files.append(ref)
    return files


def html_prep(html):
    # TODO
    return html

    head = """<head>
    <script>
    function scroll() {
        document.getElementById('scroll-mark').scrollIntoView();
    }
    </script>
    """

    body = '<body onload="scroll()">'

    html = html.replace("<head>", head)
    html = html.replace("<body>", body)

    return html


def html_prep2(html, pos):

    head = f"""<head>
    <script>
    function scroll() {{
        document.documentElement.scrollTop = {pos};
    }}

    function update()
    {{
        document.title = document.documentElement.scrollTop;
    }}
    </script>
    """

    if pos > 0:
        body = '<body onload="scroll()" onscroll="update()">'
    else:
        body = '<body onscroll="update()">'

    html = html.replace("<head>", head)
    html = html.replace("<body>", body)

    return html


def get_info_from_dtree(dtree):
    info = {}

    #docinfos = dtree.traverse(docutils.nodes.docinfo)

    for elem in dtree.traverse(siblings=True):

        if elem.tagname == "document":
            title = elem.get("title", None)
            if title:
                info["title"] = title

        if elem.tagname == "docinfo":
            for e in elem.children:

                if e.tagname == "date":
                    info["date"] = e.astext()

                if e.tagname == "author":
                    info["author"] = e.astext()

                if e.tagname == "field":
                    field_name = e.children[0].astext()
                    field_body = e.children[1].astext()
                    info[field_name] = field_body

    return info


def rst2pdf(op, rst, rst_fp, dst_fp, template, meta, curdir):
    log = op.log
    config = op.config

    log.debug("exporting {}".format(rst_fp))

    # add meta info from dtree
    dtree = rst2dtree(rst, config)
    info = get_info_from_dtree(dtree)
    meta.update(info)

    dtree = dtree_prep_ref2fp(dtree, curdir)
    tex = dtree2tex(dtree, template, meta)

    tmpdir = tempfile.mkdtemp(prefix="labnote-")

    # copytree of referenced files
    rst_dp = os.path.dirname(rst_fp)
    #print(rst_fp, rst_dp)
    files = dtree_find_files(dtree, rst_dp)
    log.debug(str(files))

    for fp in files:
        if not os.path.isabs(fp):
            #print("copy", fp)
            log.debug("copying {}".format(fp))
            os.makedirs(os.path.join(tmpdir, os.path.dirname(fp)), exist_ok=True)
            try:
                shutil.copy(os.path.join(op.model.get_base(), fp),
                            os.path.join(tmpdir, fp))
            except FileNotFoundError:
                log.warning("file not found")

    # write tex
    tex_fp = rst_fp[:-4] + ".tex"
    tex_fp = os.path.join(tmpdir, tex_fp)

    # create dirs if not existant!
    try:
        os.makedirs(os.path.dirname(tex_fp))
    except FileExistsError:
        pass

    with open(tex_fp, "w", encoding="utf-8") as f:
        f.write(tex)

    # copy template dir
    # template is abs path to template .tex file
    # remove ".tex"
    template_dir = template[:-4]
    if os.path.exists(template_dir):
        tex_dp = os.path.dirname(tex_fp)
        template_name = os.path.basename(template_dir)
        #shutil.copytree(template_dir, os.path.join(tex_dp, template_name))
        shutil.copytree(template_dir, os.path.join(tmpdir, template_name))

    #
    ok = compile_latex(log, op.config, tex_fp, tmpdir)
    if ok:
        pdf_fp = os.path.basename(tex_fp)[:-4] + ".pdf"
        shutil.copy(os.path.join(tmpdir, pdf_fp), dst_fp)
        # debug
        #if not False:
        if op.log.isEnabledFor(logging.DEBUG):
            shutil.rmtree(tmpdir)
        return True

    op.view.status_set("export failed (tex to pdf)")
    return False


def dtree2tex(dtree, template, meta):
    args = {
        #"output_encoding": "unicode",

        "_disable_config": True,
        "syntax_highlight": False,
        "tab_width": 4,

        # TODO double toc?
        #"use_latex_toc": True,  # broken
        "use_latex_toc": False,

        "table_style": ["booktabs"]
    }

    # generate defines
    defaults = {
        "title": "",
        "subtitle": "",
        "author": "",
        "topic": "",
        "date": "",
        "rev": ""
    }

    for key in defaults.keys():
        if not meta.get(key, None):
            meta[key] = defaults[key]

    defines = ""
    for key in meta.keys():
        defines += r"\newcommand{\ln" + key + "}{" + meta[key] + "}\n"
    args["latex_preamble"] = defines

    # ignore all template entries, that do not exist as files (ie default)
    if os.path.exists(template):
        args["template"] = template

    try:
        #tex = docutils.core.publish_from_doctree(dtree, writer_name="latex")
        tex = docutils.core.publish_from_doctree(
            dtree, writer_name="latex", settings=None, settings_overrides=args
        )
    except NotImplementedError:
        return ""
    return tex.decode()


def compile_latex(log, config, tex_fp, dp):
    # tex_fp: filepath relative to compile dir
    # dp: dirpath (abs?)

    #latex = config["labnote"]["latex"]
    latex = config["latex-bin"]

    log.debug("running pdflatex for {} in {}".format(tex_fp, dp))
    for i in range(0, 4):
        log.debug("compiling latex, pass {}".format(i))
        try:
            ret, out = labnote.utils.run([latex, "-halt-on-error", tex_fp], cwd=dp)
        except FileNotFoundError:
            log.error("pdflatex not installed?")
            return False

        if ret != 0:
            log.error(out)
            return False

        if "Rerun" in out:
            continue
        if "undefined references" in out:
            continue
        if "No pages of output." in out:
            continue

        return True

    log.debug("not done after {} passes".format(i))
    return False


def update_generated_files(rst, curdir, config):
    # called on file save
    return

    # TODO
    # try generating pending nodes in directive and process in transform?
    # file generation could be controlled by enabling the transform

    # OR
    # add own node? (raw directive has own node type)
    # passed to writer!
    # we dont know yet if we should generate .eps
    #   or .png in directive (during parsing!)


    # check
    # https://github.com/sphinx-contrib/plantuml/blob/master/sphinxcontrib/plantuml.py

    # create document tree
    dtree = rst2dtree(rst, config)

    # search for marked images
    for elem in dtree.traverse(siblings=True):
        try:
            name = elem.asdf
        except AttributeError:
            name = None

        if name:
            print("found", name)
            print(elem.rawsource)

