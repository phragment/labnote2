
# LabNote2

LabNote2 is a desktop wiki backed by folders with a bunch of text files.
It is intended for knowledge management using a hierarchical structure.

It supports *reStructuredText* as well as *CommonMark* (Markdown) to help you write, organize and browse your notes, reports or knowledge collections.

**Status:** in daily use, mature

The biggest advantage of rST, aside of a properly specified lightweight markup is its ability to add custom directives.
To accommodate this LabNote2 enables its users to load plugins.


## Screenshots

running on a Linux desktop

![Screenshot](demo/labnote2.png)

running on a Pinephone under Phosh

![Screenshot Phosh Pinephone](demo/labnote_phosh.jpg)

running on Windows

![Screenshot Windows](demo/labnote_windows.jpg)


## Dependencies

LabNote uses:

- [PyGObject](https://pygobject.readthedocs.io/en/latest/)
- [GtkSourceView](https://wiki.gnome.org/Projects/GtkSourceView)
- [Docutils](http://docutils.sourceforge.net/)
- [Matplotlib](https://matplotlib.org/)

optional:

- [WebKit2Gtk](https://webkitgtk.org/)
- [libspelling](https://gitlab.gnome.org/chergert/libspelling)
- [MyST Parser](https://github.com/executablebooks/MyST-Parser)



