#!/usr/bin/env python3

import os
import sys

## pre-imports (hacky?)
# argparse
import locale
import shutil

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../../src")))
from labnote.base import Application


class TestApp:

    def __init__(self):
        self.scheme = {
            "storage": {"type": "datapath", "default": "storage.json"},
            "adaptive": {"type": "bool", "default": False, "desc": "mobile mode"}
        }

        app = Application("someapp", self.scheme, self.main, self.stop)
        app.run()

    def main(self, log, settings):
        log.debug("doing stuff")

        self.settings = settings

    def stop(self):
        log.debug("stopping")


if __name__ == "__main__":

    #print(os.environ)
    #print(sys.argv)


    ## chroot
    p = os.path.abspath("test0")
    # set working dir
    os.chdir(p)
    # change root dir
    os.chroot(p)
    # drop privileges
    uid = os.getenv("SUDO_UID")
    gid = os.getenv("SUDO_GID")
    os.setgroups([])
    os.setgid(int(gid))
    os.setuid(int(uid))

    os.environ["HOME"] = "/home/user"



    ## test case
    # specify and create data directory
    app = TestApp()

    # check
    print(app.settings)
    if app.settings["storage"] != "/home/user/.local/share/someapp/storage.json":
        print("NOT ok")
    else:
        print("OK")


    # $XDG_DATA_HOME
    # $HOME/.local/share















