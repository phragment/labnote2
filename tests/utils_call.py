#!/usr/bin/env python3

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../src")))
import labnote.utils


def test1():
    print("this should return foo after 3s")
    rc, data = labnote.utils.run(["bash", "-c", "sleep 3; echo foo"])
    print(data)


def test2():
    print("this should return immediately")
    labnote.utils.start(["bash", "-c" ,"sleep 10"])
    print("ok?")


if __name__ == "__main__":
    test1()
    print("")
    test2()


