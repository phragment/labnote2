#!/usr/bin/env python3

# This file is part of LabNote2
# Copyright 2019-2024 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys
import unittest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../src")))
from labnote.refs import minify_fp2ref, shorten_ref, convert_ref2laburi, convert_laburi2fp


class Tests(unittest.TestCase):
    """directives like images

    .. image:: rst.png
      :target: rst.png

    First reference should load the image, the second should open
    the image in an external viewer for better inspection.
    This is distinguished by the load state of the webview.
    The first is a resource load, the second a site load.
    Therefore this does not need to be handled here.


    mode

    To ensure sanity uri locations are chosen to be always relative,
    to the start directory.

    As a result the mode is only used to mark if a link is outside of the
    start directory, and hence relative to the vfs root.


    references

    foo.rst
    file://foo.rst

    both are relative to the current directory

    /foo.rst
    is absolute to the start directory

    file:///foo.rst
    is absolute to root
    """

    def test_ref2laburi_1(self):
        cur = ""
        ref = "index.rst"
        uri = "labnote://index.rst"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_2(self):
        cur = ""
        ref = "sub/index.rst"
        uri = "labnote://sub/index.rst"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_3(self):
        cur = ""
        ref = "/index.rst"
        uri = "labnote:///index.rst"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_3a(self):
        cur = "sub"
        ref = "/index.rst"
        uri = "labnote:///index.rst"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_4(self):
        cur = ""
        ref = "file:///etc/resolv.conf"
        uri = "labnote:///etc/resolv.conf"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_5(self):
        cur = "sub"
        ref = "index.rst"
        uri = "labnote://sub/index.rst"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_6(self):
        cur = "sub"
        ref = "../index.rst"
        uri = "labnote://index.rst"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_7(self):
        cur = ""
        ref = "~/Documents/foo.odt"
        user = os.path.expanduser("~")
        drive, user = os.path.splitdrive(user)  # windows
        user = os.path.normpath(user)  # windows
        uri = "labnote://{}/Documents/foo.odt".format(user)
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_8(self):
        cur = "sub"
        ref = "../index.rst"
        uri = "labnote://index.rst"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_9(self):
        cur = "sub"
        ref = "../../index.rst"
        uri = "labnote://index.rst"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_10(self):
        cur = ""
        ref = "sub//index.rst"
        uri = "labnote://sub/index.rst"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_11(self):
        cur = ""
        ref = "file:///etc/nsswitch.conf"
        uri = "labnote:///etc/nsswitch.conf"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_12(self):
        cur = ""
        ref = "~/sub/../index.rst"
        user = os.path.expanduser("~")
        drive, user = os.path.splitdrive(user)  # windows
        user = os.path.normpath(user)  # windows
        h = os.path.normpath(ref.replace("~", user))
        uri = "labnote://{}".format(h)
        uri_ = convert_ref2laburi(ref, cur)
        #print(uri, uri_)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_13(self):
        cur = ""
        ref = "file://foo/bar/baz.jpg"
        uri = "labnote://foo/bar/baz.jpg"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    def test_ref2laburi_14(self):
        cur = ""
        ref = "file://foo baz.jpg"
        uri = "labnote://foo%20baz.jpg"
        uri_ = convert_ref2laburi(ref, cur)
        self.assertEqual(uri, uri_)

    # ------------------------------

    def test_laburi2fp_1(self):
        uri = "labnote://index.rst"
        path = "index.rst"
        path_ = convert_laburi2fp(uri)
        self.assertEqual(path, path_)

    def test_laburi2fp_2(self):
        uri = "labnote:///etc/nsswitch.conf"
        path = "/etc/nsswitch.conf"
        path_ = convert_laburi2fp(uri)
        self.assertEqual(path, path_)

    def test_laburi2fp_3(self):
        uri = "labnote://foo%20baz.jpg"
        path = "foo baz.jpg"
        path_ = convert_laburi2fp(uri)
        self.assertEqual(path, path_)

    # ------------------------------

    def test_minify_1(self):
        uri = "/home/user/foo/bar/baz.jpg"
        bd = "/home/user/foo"
        cd = "bar"
        fp = "baz.jpg"
        fp_ = minify_fp2ref(uri, bd, cd)
        self.assertEqual(fp, fp_)

    def test_minify_2(self):
        uri = "/home/user/foo/bar/baz.jpg"
        bd = "/home/user/foo"
        cd = ""
        fp = "bar/baz.jpg"
        fp_ = minify_fp2ref(uri, bd, cd)
        self.assertEqual(fp, fp_)

    def test_minify_3(self):
        uri = "/etc/foo/bar/baz.conf"
        bd = "/home/user/foo"
        cd = ""
        fp = "file:///etc/foo/bar/baz.conf"
        fp_ = minify_fp2ref(uri, bd, cd)
        self.assertEqual(fp, fp_)

    def test_minify_4(self):
        user = os.path.expanduser("~")
        drive, user = os.path.splitdrive(user)  # windows
        user = os.path.normpath(user)  # windows
        uri = "{}/bar/baz.conf".format(user)
        bd = "/home/thomas/notes"
        cd = ""
        fp = "file://~/bar/baz.conf"
        fp_ = minify_fp2ref(uri, bd, cd)
        self.assertEqual(fp, fp_)

    def test_shorten_ref_1(self):
        ref = "foo/index.rst"
        short = "foo/index.rst"
        short_ = shorten_ref(ref)
        self.assertEqual(short, short_)

    def test_shorten_ref_2(self):
        ref = "foo/bar/baz/index.rst"
        short = "foo/bar/baz/"
        short_ = shorten_ref(ref)
        self.assertEqual(short, short_)

    def test_shorten_ref_3(self):
        ref = "foo/bar/baz/somename.rst"
        short = "…/somename.rst"
        short_ = shorten_ref(ref)
        self.assertEqual(short, short_)

    def test_shorten_ref_3a(self):
        ref = "bar/baz/name.rst"
        short = "…/baz/name.rst"
        short_ = shorten_ref(ref)
        self.assertEqual(short, short_)

    def test_shorten_ref_4(self):
        ref = "areallyridiculousname.rst"
        short = "areallyridiculo…"
        short_ = shorten_ref(ref)
        self.assertEqual(short, short_)

    def test_shorten_ref_5(self):
        ref = ""
        short = ""
        short_ = shorten_ref(ref)
        self.assertEqual(short, short_)


if __name__ == "__main__":

    unittest.main()


