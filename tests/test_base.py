#!/usr/bin/env python3

import os
import subprocess
import sys




if __name__ == "__main__":

    if os.geteuid() != 0:
        print("must be run as root")
        sys.exit(1)

    d = os.path.abspath("tests/test_base")
    os.chdir(d)
    print(d)

    #subprocess.run(["./test0.py", "-v"])
    #print("")
    #subprocess.run(["./test1.py"])
    #print("")
    #subprocess.run(["./test2.py"])

    for p in sorted(os.listdir("./")):
        if p.endswith(".py"):
            print("\nrunning:", p)
            subprocess.run(["python", p])

    print("\ndone")




