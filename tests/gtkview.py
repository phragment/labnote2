#!/usr/bin/env python3

# This file is part of LabNote2
# Copyright 2023 Thomas Krug
#
# LabNote2 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# LabNote2 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import os
import signal
import sys
import threading
import traceback
import time

import gi
gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, GLib

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../src")))
import labnote.transform
import labnote.gtkview
import labnote.gtkdtree


cache_math = labnote.gtkdtree.MathCache()
cache_img = labnote.gtkdtree.ImageCache()


class DummyView:

    def __init__(self, fp):
        self.log = logging.getLogger()
        self.config = {"styles-dir": "config/styles/", "style-gtkview": "gtk.css", "benchmark": False}
        self.fp = fp


class Application:

    def __init__(self, view):
        self.view = view
        self.app = Gtk.Application.new(None, 0)
        self.app.connect("activate", self.activate)

    def run(self):
        self.app.run()

    def activate(self, app):
        self.win = MainWindow(app, self.view)


class MainWindow:

    def __init__(self, app, view):
        self.view = view

        self.win = Gtk.ApplicationWindow.new(app)
        self.win.connect("close-request", self.on_window_close_request)

        self.win.set_title("Test GtkView - " + self.view.fp)
        self.win.set_size_request(400, 600)

        self.test_view = labnote.gtkview.GtkView(self.view, "ident")
        self.win.set_child(self.test_view.get_wdgt())

        #self.win.set_visible(True)
        self.win.present()

        print("MainWindow done")

    def on_window_close_request(self, window):
        self.win.close()


def run_test(app, fp):
    time.sleep(1)

    try:
        with open(fp, "r") as f:
            rst = f.read()
    except FileNotFoundError:
        rst = ""

    config = {"verbose": False, "benchmark": False}

    dtree = labnote.transform.rst2dtree(rst, config)
    dtree = labnote.transform.dtree_prep_ref2fp(dtree, "tests/rst")

    entries = labnote.gtkdtree.convert(dtree, cache_img, cache_math)

    GLib.idle_add(app.win.test_view.update, entries)


def run(fp):
    view = DummyView(fp)
    app = Application(view)

    thread = threading.Thread(target=run_test, args=(app, fp))
    thread.start()

    try:
        app.run()
    except KeyboardInterrupt:
        os.kill(os.getpid(), signal.SIGKILL)
        pass


def on_exception(etype, value, trace):
    print("crash in {}".format(threading.current_thread().name))
    #print(etype)
    #print(value)
    print("\n".join(traceback.format_exception(etype, value, trace)))
    os.kill(os.getpid(), signal.SIGKILL)


def on_thread_exception(args):
    on_exception(args.exc_type, args.exc_value, args.exc_traceback)


if __name__ == "__main__":
    sys.excepthook = on_exception
    threading.excepthook = on_thread_exception

    dp = "tests/rst"
    tests = os.listdir(dp)

    for test in sorted(tests):
        if not test.endswith(".rst"):
            continue
        print(test)
        run(os.path.join(dp, test))



