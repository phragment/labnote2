
:abstract:     some abstract
:address:      some address
:author:       some author
:authors:      author1; author2
:contact:      some contact
:copyright:    some copyright
:date:         some date
:dedication:   some dedication
:organization: some organization
:revision:     some revision
:status:       some status
:version:      some version

above are known bibliographic fields

Docinfo is represented in reStructuredText by a field_list in a bibliographic context:
the first non-comment element of a document, after any document title/subtitle.
The field list is transformed into a docinfo element and its children by a transform.


So these are going to be field lists:


:authors: one

          two

:authors: one; two

:author: Some Author

:date: 2020



:topic: Some Topic

  field lists can contain paragraphs



Some paragraph to catch escaping text.



Some more text.

