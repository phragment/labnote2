

.. |reST| replace:: reStructuredText

Yes, |reST| is a long word, so I can't blame anyone for wanting to
abbreviate it.


Lets try it this way... :abbreviation:`test`


"The abbreviation element is not exposed in default restructured text.
It can only be accessed through custom roles."


Now for an :acronym:`acronym`?



