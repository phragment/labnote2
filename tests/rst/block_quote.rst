
Block Quotes


  Kelly was a seaman, and his life on the water followed a strict routine,
  which meant observing all the safety rules
  that had been written in the blood of less careful men.

  -- from *Without Remorse* by **Tom Clancy**




End
