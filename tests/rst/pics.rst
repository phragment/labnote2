
image test
==========


.. figure:: images/title.png
  :target: images/title.png

  image should be a clickable target


missing image

.. image:: notfound.png


explicitly setting size

.. image:: notfound.png
  :height: 100px

  em, px, in, cm, mm, pt, pc, ""


landscapes

.. image:: images/arrow-land.jpg


portrait

.. image:: images/arrow-port.jpg


portrait from landscape (exif orientation)

.. image:: images/arrow-port-er.jpg




small versions

.. figure:: images/arrow-land.tmb.jpg
  :target: images/arrow-land.jpg


.. figure:: images/arrow-port.tmb.jpg
  :target: images/arrow-port.jpg




inline images: |cg| currently only work with rsthtml renderer

.. |cg| image:: images/title.png
  :width: 50px



