
DESTDIR=/
BINDIR=$(DESTDIR)/usr/bin
SYSCONFDIR=$(DESTDIR)/etc
OPTDIR=$(DESTDIR)/opt
DATADIR=$(DESTDIR)/usr/share

.PHONY: all run run_phone run_gtk clean edit tests test_base lint lint_extra install install_desktop install_phone

all: run

edit:
	gedit -s src/labnote.py src/labnote/base.py src/labnote/operator.py src/labnote/events.py src/labnote/transform.py src/labnote/model.py src/labnote/view.py src/labnote/webview.py src/labnote/gtkview.py src/labnote/gtkdtree.py src/labnote/refs.py src/labnote/git.py src/labnote/utils.py 2> /dev/null &

tests:
	./tests/refs.py
	./tests/utils_call.py

test_gtkview:
	./tests/gtkview.py

test_base:
	./tests/test_base.py

run:
	./src/labnote.py -v -c config/config_desktop.ini demo

run_phone:
	./src/labnote.py -v -c config/config_phone.ini demo

run_gtk:
	./src/labnote.py -v -c config/config_gtk.ini demo

clean:
	-rm -r src/labnote/__pycache__
	-rm -r config/directives/__pycache__

lint:
	# Ignored Errors and Warnings:
	# E265 block comment should start with '# '
	# W391 blank line at end of file
	-pycodestyle --ignore=E265,W391 --max-line-length=120 src/*

lint_extra:
	-pyflakes src/*
	#-pylint src/*
	#-flake8 src/*

install:
	mkdir -p $(BINDIR) $(SYSCONFDIR)/labnote2 $(OPTDIR)/labnote2 $(DATADIR)/applications
	cp -r src/* $(OPTDIR)/labnote2/
	cp -r config/* $(SYSCONFDIR)/labnote2/
	install -Dm755 labnote2.sh $(BINDIR)/labnote2
	cp *.desktop $(DATADIR)/applications/

uninstall:
	-rm -r $(SYSCONFDIR)/labnote2
	-rm -r $(OPTDIR)/labnote2
	-rm $(BINDIR)/labnote2
	-rm $(DATADIR)/applications/labnote2.desktop


