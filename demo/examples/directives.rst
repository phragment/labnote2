:title: Title
:subtitle: Subtitle
:author: Authors
:date: 1900-01-01
:topic: Topic



Small rST Directive Reference
-----------------------------

Lets start with some text.
Some more.

Another paragraph.


- a
- bullet
- list

another bullet list

- with

  - two
  - levels

    - or three

      and a block quote? no a paragraph

      another paragraph inside the list, both should be indented (TODO)

and

1. an
2. enumeration

   1. foo
   2. bar

3. with second level



And now a literal block::

  some literal text


Just a paragraph

  lets try a block quote

    and another



.. sidebar:: Sidebar

  some stuff


.. csv-table:: some table
  :header: "Column 1", "Column 2", "Column 3"

  Foo, Bar, Baz
  Foo, Bar, Baz
  Foo, Bar, Baz
  Foo, Bar, Baz


.. math::

  c^2 = a^2 + b^2

  e^{j \pi} = -1

  \phi = arctan ( \frac{Im}{Re} )


.. code::

  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.

.. code:: python

  import antigravity

  print("test")


.. code:: c

  #include <stdio.h>

  int main() {
    print("foo\n");
    return 0;
  }


.. code:: sh

  if [ -x "$FILE" ];
    then
      echo ""
  fi


.. image:: rst.png
  :target: rst.png


.. figure:: ../labnote2.png
  :target: ../labnote2.png

  figure with caption


