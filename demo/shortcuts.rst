
Shortcuts
=========

============  ===============
**Shortcut**  **Description**
------------  ---------------
Ctrl-s        saves the currently open file


Ctrl-v        this will not just paste text, but also create image files
              from clipboard data and insert a corresponding reference

              pasting file links allows for copying files directly to notes

Ctrl-Shift-v  pastes the clipboard and prepends spaces for literal blocks


Ctrl-Tab      switch between tabs

Ctrl-w        close tab


Ctrl-Shift-e  export current file to pdf (using latex)

Ctrl-r        refresh view

Ctrl-Shift-r  reload file


Ctrl-f        search in current file

Ctrl-Shift-f  recursively search over all files in start directory


Alt-e         focus editor

Ctrl-space    scroll viewer to current line in editor

Middle Click  scroll editor to viewer position (using gtk view)

F7            check spelling
============  ===============

